@extends('layouts.app')

@section('content')
<div class="container">
    <h1 style="margin-top: 35px;">Conditions générales d'utilisation du SiteWeb mivadias</h1>
    <h3>Article 1. Préambule</h3>
    <p> www.mivadias.com  est un produit de la  société e-solux group anonyme à conseil d’administration au capital de 1 000000 FCFA  immatriculée au registre de commerce de Lomé, sous le numéro 03477PM2021/2021, dont le siège social sis à Agoè 1, rue Léo 2000, Lomé Togo agréé en qualité d’établissement de service paiement , publiée au Bulletin officiel , dûment représentée par son Directeur Général.</p><br/>
    <p>E-solux group édite le site web accessible à l’adresse : https://www.mivadias.com (le « Site »). </p><br/>
    <p>Les présentes conditions d'utilisation s'appliquent à l’utilisation de ce site Web et des applications mobiles de mivadias, et ne modifient pas les dispositions des autres accords conclus par ailleurs par l’Utilisateur avec mivadias.</p>
    <h3>Article 2. Accès au Site</h3>
    <p> L'accès au site www.mivadias.com se fait sans aucune restriction, il est destiné à tous les publics. Le Site a pour objectif de faire une présentation des services de mivadias et permet également d’effectuer des services pour la diaspora en ligne. &nbsp;&nbsp;
    L’accès au Site est gratuit, hors fournisseurs d’accès Internet et hors coût de communications téléphoniques qui sont facturés directement par les opérateurs &nbsp;&nbsp;
    L’Utilisateur est informé que le Site est fourni sur la base d’un service « en l’état de l’art » et accessible en fonction de sa disponibilité, 24 h sur 24 et 7 jours sur 7, sauf en cas de force majeure, difficultés informatiques, difficultés liées aux réseaux de télécommunications ou autres difficultés techniques.&nbsp;&nbsp;
    La responsabilité de mivadias ne pourra être recherchée ni retenue en cas d’indisponibilité temporaire ou totale de tout ou partie de l’accès au Site, d’une difficulté liée au temps de réponse, et d’une manière générale, d’un défaut de performance quelconque.&nbsp;&nbsp;
    Mivadias se réserve le droit, sans préavis, ni indemnité, de fermer temporairement ou définitivement le Site ou l’accès à un ou plusieurs Services pour effectuer une mise à jour, des modifications ou changement sur les méthodes opérationnelles, les serveurs et les heures d’accessibilité, sans que cette liste ne soit limitative.&nbsp;&nbsp; 
    Mivadias se réserve le droit de compléter ou de modifier, à tout moment, ses services et son Site en fonction de l’évolution de la technologie.
    </p>
    <h3>Article 3. Informations mises à dispositions sur le Site </h3>
    <p>Les informations fournies sur le Site le sont à titre purement informatif et indicatif et constituent une présentation des services de mivadias. Par conséquent, les informations diffusées sur le site ne doivent en aucun cas être interprétées comme un démarchage et ne constituent pas non plus une offre de services et/ou de produits de mivadias ou de toute autre société mentionnée sur le Site.&nbsp;&nbsp;
    Mivadias  s'efforce d'assurer l'exactitude et la mise à jour des informations diffusées sur ce Site, dont elle se réserve le droit de modifier et corriger le contenu à tout moment et sans préavis. Elle ne peut toutefois en garantir la complétude, la précision, l'exhaustivité ou l'absence de modification par un tiers.&nbsp;&nbsp;
    Mivadias décline en conséquence toute responsabilité :
        <ul>
            <li>en cas d'imprécision, inexactitude, erreur ou omission portant sur des informations disponibles sur le Site ;</li>
            <li>en cas d’absence de disponibilité des informations ;</li>
            <li>pour tous dommages, directs et/ou indirects, quelles qu'en soient les causes, origines, nature ou conséquences, provoqués à raison de l'accès de quiconque au Site ou de l'impossibilité d'y accéder ;</li>
            <li>en cas d'utilisation du Site et/ou du crédit accordé à une quelconque information provenant directement ou indirectement du Site ;</li>
            <li>en cas de décisions prises sur la base d'une information contenue sur le Site, et de l'utilisation qui pourrait en être faite par des tiers.</li>
        </ul>
    Il revient par conséquent à toute personne désireuse de se procurer le service présenté sur le présent Site de contacter le service concerné afin de s’informer de la disponibilité du service ou produit en question ainsi que des conditions contractuelles et des tarifs qui lui sont applicables. 
    </p>
    <h3>Article 4. Sécurité</h3>
    <p>Le Site est un système de traitement automatisé de données. Tout accès frauduleux, notamment aux espaces personnels qu’il renferme, est interdit et sanctionné pénalement.&nbsp;&nbsp;
    Mivadias fait ses meilleurs efforts, conformément aux règles de l’art, pour sécuriser le Site. Eu égard à la complexité de l’internet, elle ne saurait assurer une sécurité absolue.&nbsp;&nbsp;
    L’Utilisateur déclare accepter les caractéristiques et limites de l’internet et, notamment, il reconnaît avoir connaissance de la nature du réseau de l’internet, et en particulier, de ses performances techniques et des temps de réponse pour consulter, interroger ou transférer les données.&nbsp;&nbsp;
    L’Utilisateur a conscience que les données circulant sur internet ne sont pas nécessairement protégées, notamment contre les détournements éventuels.&nbsp;&nbsp;
    L’Utilisateur accepte de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par des éventuels virus sur le réseau internet.&nbsp;&nbsp;
    Par ailleurs, mivadias  se réserve le droit de suspendre l'accès au Site à l’Utilisateur si elle devait relever des faits laissant présumer l'utilisation frauduleuse ou la tentative d'utilisation frauduleuse de ses services ou que l’Utilisateur a communiqué des informations inexactes se rapportant à son identité. 
    </p>
    <h3>Article 5. Responsabilité</h3>
    <p>L’Utilisateur s’engage à n’utiliser le Site et les informations auxquelles il aurait accès que dans les seules conditions définies par mivadias.&nbsp;&nbsp;
    Par ailleurs, l’Utilisateur s’engage à (i)ne pas perturber l’usage que pourraient faire les autres utilisateurs du Site et de ne pas accéder aux espaces de tiers (ii) à ne commettre aucun acte pouvant mettre en cause la sécurité informatique mivadias  ou des autres utilisateurs ou visiteurs (iii) à ne pas interférer ou interrompre le fonctionnement normal du Site.&nbsp;&nbsp;
    L’Utilisateur s'interdit notamment de modifier, copier, reproduire, télécharger, diffuser, transmettre, exploiter commercialement et/ou distribuer de quelque façon que ce soit les services, les pages du Site, ou les codes informatiques des éléments composant les services et le Site.&nbsp;&nbsp;
    </p>
    <h3>Article 6. Propriété intellectuelle</h3>
    <p>Les présentes Conditions générales n’emportent aucune cession d’aucune sorte de droits de propriété intellectuelle sur les éléments appartenant à mivadias au bénéfice de l’Utilisateur.&nbsp;&nbsp;
    Le Site, les marques, les dessins, les modèles, les images, les textes, les photos, les logos, les chartes graphiques, les logiciels, les moteurs de recherche, les bases de données et les noms de domaine, sans que cette liste ne soit exhaustive, sont la propriété exclusive de mivadias.&nbsp;&nbsp;
    Sont expressément interdits, toute reproduction, représentation, adaptation, exploitation, distribution, diffusion, utilisation commerciale, traduction, arrangement ou transformation de tout ou partie des contenus ou tout autre élément figurant sur le site sur quelque support que ce soit et par tout procédé que ce soit. Ces agissements sont susceptibles de constituer des actes de contrefaçon sanctionnés pénalement et civilement, et engagent la responsabilité de leur auteur. 
    </p>
    <h3>Article 7. Liens hypertexte</h3>
    <p>Le Site peut inclure des liens hypertextes vers des sites tiers présents sur le réseau Internet. Mivadias ne peut contrôler la nature ou le contenu des ressources constituées par ces sites tiers. En conséquence, la responsabilité de Cash Plus ne saurait être engagée concernant un site tiers auquel l'utilisateur aurait accès via le Site.&nbsp;&nbsp;
    La responsabilité de mivadias ne pourra pas être recherchée ni retenue si d’éventuelles collectes et transmission de données personnelles, d’installation de cookies ou tout autre procédé tendant aux mêmes fins, sont effectués par des sites vers lesquels pointent des liens hypertextes accessibles depuis le Site ou accessibles par rebond.
    </p>
    <h3>Article 8. Données à caractère personnel</h3>
    <p>Les informations recueillies sur le site www.mivadias.com font l’objet d’un traitement dont les finalités sont les suivantes :
        <ul>
            <li>la commercialisation des services proposés par mivadias ;</li>
            <li>conformité aux lois et réglementations régissant l’activité mivadias ;</li>
            <li>opérations marketing, études de marché, amélioration de l’expérience clients.</li>
        </ul>
     Les destinataires des données sont mivadias, ses filiales ainsi que les partenaires de mivadias dans le cadre de la fourniture des services commercialisés.&nbsp;&nbsp;
     Vous pouvez également, pour des motifs légitimes, vous opposer à ce que les données qui vous concernent fassent l’objet d’un traitement.&nbsp;&nbsp;
     Le traitement relatif aux demandes des agents partenaires  a été notifié et a fait l’objet du récépissé n°  n°2021/E/177 du 07/04/2021.
    </p>
    <h3>Article 9. Convention de Preuve</h3>
    <p>L’acceptation des Conditions générales par voie électronique a, entre les parties, la même valeur probante que l’accord sur support papier.&nbsp;
    Les registres informatisés et conservés dans les systèmes informatiques sont conservés dans des conditions raisonnables de sécurité et considérés comme les preuves des communications intervenues entre les parties.
</p>
    <h3>Article 10. Opposabilité</h3>
    <p>L’utilisation du Site de mivadias, y compris la consultation, et l’utilisation des informations mises à disposition implique l’acceptation complète et entière des présentes conditions générales d’utilisation. Tout usage du Site par l’Utilisateur après les modifications des Conditions générales d’utilisation vaut acceptation sous réserve par ce dernier.&nbsp;
    Mivadias se réserve le droit d’apporter aux présentes Conditions générales toutes les modifications qu’elle juge nécessaires et utiles, et la possibilité de modifier en tout ou partie le contrat afin de l'adapter, notamment, aux évolutions de son exploitation, et/ou à l'évolution de la législation et/ou aux évolutions des services proposés. Les Conditions générales figurant en ligne sur le Site prévalent sur toute version imprimée de date antérieure. 
    </p>
    <h3>Article 11. Loi Applicable et Juridiction Compétente</h3>
    <p>Les présentes Conditions Générales sont régies par la loi marocaine. Tout litige portant sur l’interprétation ou l’exécution des présentes sera soumis aux tribunaux compétents.</p>
</div>

@stop
