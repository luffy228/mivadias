@extends('main')

@section('content')

<style>
    .imgCard {
        height: 360px;
    }

</style>
<div class="row">
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
       
    </div>
    <section>
        <div class="col-xl-12 col-lg-12">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                        <p> Les commandes </p>
                           
                    </div>
                </div>
                <!-- Card Body -->
                 <div class="d-flex justify-content-end mt-3">
                    <label class="mr-3">Recherche</label>
                    <input type="text" id="searchvalue" name="search" class="rounded mr-3" value="{{ $valsearch }}">
                    <a href="#" style=" color: #858796" onclick="serchorder()"><i class="fas fa-fw fa-search mr-3" aria-hidden="true"></i></a>
                    <label class="mr-3">Status</label>
                    <select id="statusOrd" onchange="operation()">
                        <option value="Payé" @if ($status == 'Payé')  selected @else @endif >En cours de traitement</option>
                        <option value="Validée" @if ($status == 'Validée')  selected @else @endif>Traité</option>
                        <option value="tous" @if ($status == 'tous')  selected @else @endif>Tous</option>
                    </select>

                </div>
                <div class='ml-3 col-xl-12 col-lg-12 pt-3 pb-3'>
                    <table class='table'>
                        <thead>
                            <tr>
                                <th>Service</th>
                                <th>Nom Client</th>
                                <th>Date
                                    <i class="fas fa-fw fa-caret-down" aria-hidden="true" onclick="ordersby('desc')" style="position: absolute; margin-top: 9px; "></i>
                                    <i class="fas fa-fw fa-caret-up" aria-hidden="true" onclick="ordersby('asc')" style="position: absolute; margin-top: 0px; "></i>
                                </th>
                                <th>Quantité</th>
                                <th>PU</th>
                                <th>Frais</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Date Validation</th>
                                <th>Facture</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->getServiceTitle() }}</td>
                                <td>{{ $order->getUsersName() }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>{{ $order->quantity }}</td>
                                <td>{{ $order->montant }}</td>
                                <td>{{ $order->frais }}</td>
                                <td>{{ $order->montant_total }}</td>
                                <td>{{ $order->status }}</td>
                                <td>{{ $order->getDateValidation() }}</td>
                                <td>
                                    @if ($order->url_fact)
                                    <a href="{{ $order->url_fact }}">Telecharger</a>
                                    @endif
                                </td>
                                <td><a href="{{ route('gerance.order',['id'=>$order->id]) }}" >Details</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                
                </div>
                <div class="d-flex justify-content-end">
                    {!! $orders->links() !!}
                </div>
            </div>
        </div>
    </section>
</div>


</div>

<script type="text/javascript">
       
    function operation() {
        var statusOrd = $('#statusOrd').val();
        var searchvalue = $('#searchvalue').val();
        if(!searchvalue){
            searchvalue = "val";
        }
        var url = "{{ route('gerance.ordersOperation',['action'=>'status','value'=>'val', 'status'=>'valeur']) }}".replace("val", searchvalue).replace("valeur", statusOrd);
        document.location.href = url;
    }

    function serchorder(){
        var statusOrd   = $('#statusOrd').val();
        var searchvalue = $('#searchvalue').val();
        if(!searchvalue){
            searchvalue = "vide";
        }
        var url = "{{ route('gerance.ordersOperation',['action'=>'search','value'=>'val', 'status'=>'valeur']) }}".replace("val", searchvalue).replace("valeur", statusOrd);
        document.location.href = url;
    }

    function ordersby(by){
        
        var valstatus   = $('#statusOrd').val();
        var valsearch = $('#searchvalue').val();
        if(!valsearch){
            valsearch = "vide";
        }
        var url = "{{ route('gerance.ordersby',['ordre'=>'byorder','status'=> 'valstatus','input'=>'valsearch']) }}".replace("valstatus", valstatus).replace("valsearch", valsearch).replace("byorder", by);
        document.location.href = url;
    }
    
</script>
@stop
