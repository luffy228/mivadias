@extends('admin.main')

@section('content')

<style>
    .imgCard {
        height: 360px;
    }

</style>
<div class="row">
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
       
    </div>
    <section>
        <div class="col-xl-12 col-lg-12  px-0 px-lg-1">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-0 py-lg-3 py-3 d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                        <p> Les Clients </p>
                           
                    </div>
                </div>
                <!-- Card Body -->
                <div class='ml-lg-3 ml-0 col-xl-12 col-lg-12 pt-3 pb-3 text-nowrap'>
                    <table class='table table-responsive table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Date ajout</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </section>
</div>


</div>
@stop
