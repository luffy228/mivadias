@extends('admin.main')
@section('content')

<style>
    .imgCard {
        height: 360px;
    }

</style>
<div class="row">
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">GESTION DES SERVICES </h1>
    </div>
    <section>
        <div class="col-xl-12 col-lg-12 px-0 px-lg-1">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-0 py-lg-3  d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                    <p> Au total {{ count($services) }} Service(s) </p>
                    <button type="button" class="btn btn-primary  mx-2"  data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus" aria-hidden="true"></i></button>
                       
                           
                    </div>
                </div>
                <!-- Card Body -->
                <div class='ml-lg-3 ml-0 col-xl-12 col-lg-12 pt-3 pb-3 text-nowrap'>
                    <table class='table table-responsive table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Titre</th>
                                <th>Date Création</th>
                                <th>Frais(%)</th>
                                <th>status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td>{{ $service->id }}</td>
                           
                                <td><a href="#" data-toggle="modal" data-target="#myModal_{{ $service->id }}">{{ $service->title }}</a></td>
                           
                                <td>{{ $service->created_at }}</td>
                                <td>{{ $service->fee }}</td>
                                <td>
                                    @if ($service->status)
                                        Activé
                                    @else
                                        Désactivé
                                    @endif
                                </td>
                                <td>
                                    @if ($service->status)
                                            <a href="{{ route('admin.changestatus',['id'=>$service->id,'status'=>'0']) }}" 
                                            onclick="event.preventDefault();
                                                            document.getElementById('status-form{{ $service->id }}').submit();">Désactiver</a>
                                            <form id="status-form{{ $service->id }}" action="{{ route('admin.changestatus',['id'=>$service->id,'status'=>'0']) }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    @else
                                        <a href="{{ route('admin.changestatus',['id'=>$service->id,'status'=>'1']) }}" 
                                            onclick="event.preventDefault();
                                                            document.getElementById('status-form{{ $service->id }}').submit();">Activer</a>
                                            <form id="status-form{{ $service->id }}" action="{{ route('admin.changestatus',['id'=>$service->id,'status'=>'1']) }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </section>
</div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-xl">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                
                <h4 class="modal-title">Ajouter une service</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.storeform') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="title" class="control-label">Nom du service</label>
                        <input class="form-control" name="title" type="text" value="" id="title">
                    </div>
                    <div class="form-group">
                        <label for="fee" class="control-label">Frais de commissions</label>
                        <input class="form-control" name="fee" type="number" value="" id="fee">
                    </div>
                    <div class="form-group">
                        <label for="image" class="control-label">Image de presentation</label>
                        <input class="form-control" name="image" type="file" value="" id="image">
                    </div>
                    <table class='table'>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Titre</th>
                                <th>Type</th>
                                <th>Label</th>
                                <th>Valeur par Defaut</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($formulaires as $formulaire)
                            <tr>
                                <td><input type="checkbox" name="choix[]" value="{{  $formulaire->id }}"/></td>
                                <td>{{ $formulaire->libelle }}</td>
                                <td>{{ $formulaire->type }}</td>
                                <td><input class="form-control" name="label_{{  $formulaire->id }}" type="text" value="" id="label_{{  $formulaire->id }}"></td>
                                <td><input class="form-control" name="default_{{  $formulaire->id }}" type="text" placeholder="" value="" id="default_{{  $formulaire->id }}"></td>
                                <td>
                                @if ($formulaire->type === 'select')
                                    <input class="form-control" name="type_serv_{{  $formulaire->id }}" type="text" placeholder="Ex: Choix;Choix2" value="" id="type_serv_{{  $formulaire->id }}">
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <input type='submit'  class="btn btn-primary" value='Sauvegarder' />
                </form>
            
            </div>

            </div>
        </div>
    </div>

    @foreach ($detail_service as $detail)
        <div class="modal fade" id="myModal_{{ $detail['idservice'] }}" role="dialog">
            <div class="modal-dialog modal-xl">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                
                        <h4 class="modal-title">Modifier une service</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
        
                    <div class="modal-body">
              
                        <form action="{{ route('admin.modifservices', ['id' => $detail['idservice'] ]) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf
                         
                                <div class="form-group">
                                    <label for="title" class="control-label">Nom du service</label>
                                    <input class="form-control" name="title" type="text"  id="title" value="{{ $detail['title'] }}"/>
                                </div>
                                <div class="form-group">
                                    <label for="fee" class="control-label">Frais de commissions</label>
                                    <input class="form-control" name="fee" type="number" value="{{ $detail['fee'] }}" id="fee"/>
                                </div>
                                <div class="form-group">
                                    <label for="image" class="control-label">Image de presentation : image existant( {{ $detail['img_url'] }} )</label>
                                    <div> <img src="{{asset($detail['img_url']) }}" class="imgCard" style="width: 400px;height: 200px;"></div>
                                     <input class="form-control" name="image" type="file" value="" id="image">
                                </div>
                      
                  
                                <table class='table'>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Titre</th>
                                        <th>Type</th>
                                        <th>Label</th>
                                        <th>Valeur par Defaut</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($detail['formService'] as $formulaire)
                                    <tr>
                                        <td><input type="checkbox" name="choix[]" value="{{ $formulaire['formulaire_id'] }}"/></td>
                                        <td>{{ $formulaire['libelle'] }}</td>
                                        <td>{{ $formulaire['type'] }}</td>
                                 
                                         <td><input class="form-control" name="label_{{  $formulaire['formulaire_id'] }}" type="text" value="{{ $formulaire['intitule'] }}" id="label_{{  $formulaire['formulaire_id'] }}"></td>
                                        <td><input class="form-control" name="default_{{  $formulaire['formulaire_id'] }}" type="text" placeholder="" value="{{ $formulaire['default'] }}" id="default_{{  $formulaire['formulaire_id'] }}"></td>
                                   
                                        <td>
                                           
                                        @if ($formulaire['type'] === 'select')
                                            <input class="form-control" name="type_serv_{{  $formulaire['formulaire_id'] }}" type="text" placeholder="Ex: Choix;Choix2" value="" id="type_serv_{{  $formulaire['formulaire_id'] }}">
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            
                            <input type='submit'  class="btn btn-primary" value='Modifier' />
                        </form>
                
                    </div>
            
                </div>
            </div>
        </div>
    @endforeach
</div>
@stop
