@extends('layouts.app')

@section('content')
<style>
   /* .form_little {
       margin-left: 28% !important;
    }

</style>
<div class="container" style="margin-top: 30px !important;">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 div_title">
        <h1 class="h3 mb-0 text-gray-800">{{ $title }}</h1>
    </div>
    <section>
        <div class="col-xl-12 col-lg-12 div_form">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                        <p> Entrer vos informations </p>
                           
                    </div>
                </div>
                <!-- Card Body -->
                <div class="ml-3 col-xl-5 col-lg-4 pt-3 pb-3 form_little align-self-center ">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    window.onload=function(){
        loadCurrency();
        $('#option').change(function(){
            
           loadCurrency();
        });

        $('#duree').change(function(){
            
           loadCurrency();
        });

        $('#montant').change(function(){
            
           loadCurrency();
        });
      
    };

    function loadCurrency(){
        var element = document.getElementById("option");
        var montant = document.getElementById("montant");
        var url = '';
        if(typeof(element) != 'undefined' && element != null)
        {
            url = "../choixAmount/"+$('#option').val()+"/"+$('#duree').val();
        }else{
             url = "../currency/"+montant.value;
                
        }
         $.ajax({ 
                type: "GET",
                url: url,      
                success: function (data) {
                    data = JSON.parse(data);
                    $('#montant').val(data['data_one']);
                    $('#frais').val(data['data_two']);
                    $('#montant_total').val(data['data_three']);
                    $('#montant_total_euro').val(data['data_euro']);
                    $('#montant_total_dollar').val(data['data_usd']);
                }
            });
    }

</script>


@stop
