@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 35px;">
    <h1>Nous contacter</h1>
    <p>Vous avez des questions ? Vous souhaitez nous adresser un message ?<br/><br/>
     Écrivez nous via ce formulaire et soumettez le à nos équipes. Nous nous engageons à vous répondre dans les plus brefs délais !<br/><br/>
     Retrouvez nous également via les différents canaux sur lesquels nous sommes présents.<br/><br/>
     Facebook, LinkedIn, whatsapp, Twitter, Tik Tok, Youtube et bien plus encore...<br/><br/>
     Les icônes ci-dessous vous mèneront directement jusqu’à nous.
    </p>
    <h3 style="text-align: center;">Formulaire de contact</h3>
    <div class="ml-3 col-xl-5 col-lg-4 pt-3 pb-3 form_little align-self-center ">
                    {!! form($form) !!}
    </div>

</div>

</script>


@stop
