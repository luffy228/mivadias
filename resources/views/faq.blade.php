@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 35px;">
    <P>
        <strong>Foire aux questions</strong><br/>
        Informations Générales<br/><br/>
        <strong>Qu’est-ce que mivadias ?</strong><br/>
        Mivadias est une plateforme de services Agréé  qui  propose une panoplie de services .<br/><br/>
        En effet vous pouvez y gérer votre compte, payer vos factures  depuis l’étranger vers le Togo grâces à nos partenaires du transfert international ( visa, MasterCard, Western Union, MoneyGram, Ria…), payer vos factures d’eau et d’électricité, vos abonnements internets, télés, payer vos achats e-commerce et bien plus encore…<br/><br/>
        <strong>Qui peut utiliser les services de mivadias ?</strong><br/>
        Que vous soyez bancarisé ou non, les services mivadias sont mis à la disposition de toute personne se trouvant hors du Togo.<br/><br/>
        Quels sont les horaires mivadias?<br/>
        Nos services sont fonctionnels 24h/24h et 7jrs/7.<br/><br/>
        <strong>Comment trouver mivadias?</strong><br/>
        Vous pouvez trouver mivadias en allant sur internet et vous connecter sur www.mivadias.com<br/><br/>
        <strong>Comment utiliser mivadias ?</strong>
        Une fois connecté, cliquer sur le service désiré, et remplissez les informations du service souhaité, passer au paiement et le traitement s’effectue en moins de 15 minutes.<br/><br/>
        <strong>Comment nous contacter ?</strong><br/>
        Utilisez la rubrique Nous contacter pour nous écrire ou nous joindre via l’un des différents canaux sur lesquels nous sommes présents.<br/>
        Nous nous engageons à vous répondre dans les plus brefs délais.
    </P>
</div>

@stop
