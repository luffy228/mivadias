@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="checkout.js" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">   
@endsection
@section('content')

<br>
<br>
<br>

<form id="payment-form">
    <div id="payment-element">
      <!--Stripe.js injects the Payment Element-->
    </div>
    <button id="submit">
      <div class="spinner hidden" id="spinner"></div>
      <input type="hidden" name="montant" value="{!!$montant!!}" id="montant"/>
      <input type="hidden" name="description" value="{!!$description!!}" id="description"/>
      <input type="hidden" name="order" value="{!!$idorder!!}" id="order"/>
      <button class="btn btn-primary btn-lg btn-block" id="button-text" >Payer s</button>
    </button>
    <div id="payment-message" class="hidden"></div>
  </form>

@stop

@section('js')

@endsection
