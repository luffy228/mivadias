@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <h2>Informations de votre carte</h2>
                <br>
            </div>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 
    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
 
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-3">
 
        </div>
        <div class="col-lg-6">
            <form  action="{{url('payment')}}"  data-cc-on-file="false" data-stripe-publishable-key="pk_test_fgfl5fgfg5f8g5fg8fg5" name="frmStripe" id="frmstripe" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <label>PROPRIETAIRE DE LA CARTE</label>
                        <input class="form-control" size="4" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <label>NUMÉRO DE CARTE</label>
                        <input autocomplete="off" class="form-control" size="20" type="text" name="card_no">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>CRYPTOGRAMME VISUEL</label>
                        <input autocomplete="off" class="form-control" placeholder="ex. 311" size="3" type="text" name="cvv">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>MOIS  EXPIRATION</label>
                        <input class="form-control" placeholder="MM" size="2" type="text" name="expiry_month">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>ANNEE EXPIRATION</label>
                        <input class="form-control" placeholder="YYYY" size="4" type="text" name="expiry_year">
                    </div>
                </div>
                <input type="hidden" name="montant" value="{!!round($montant, 0)*100!!}"/>
                <input type="hidden" name="description" value="{!!$description!!}"/>
                <input type="hidden" name="idorder" value="{!!$idorder!!}"/>
                <x-input type="hidden" name="payment_method" id="payment_method" />
                <!-- Stripe Elements Placeholder -->
                <div id="card-element"></div>

                <x-button class="mt-3" id="submit-button">Payer</x-button>
            </form>
        </div>
    </div>
</body>
</html>


@stop

@section('js')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
  
<script>
    const stripe = Stripe(" {{ env('STRIPE_KEY') }} ");
    const elements = stripe.elements();
    const cardElement = elements.create('card', {
        classes: {
            base: 'StripeElement bg-white w-1/2 p-2 my-2 rounded-lg'
        }
    });
    cardElement.mount('#card-element');
    const cardButton = document.getElementById('submit-button');
    cardButton.addEventListener('click', async(e) => {
        e.preventDefault();
        const { paymentMethod, error } = await stripe.createPaymentMethod(
            'card', cardElement
        );
        if (error) {
            alert(error)
        } else {
            document.getElementById('payment_method').value = paymentMethod.id;
        }
        document.getElementById('form').submit();
    });
</script>
@endsection
