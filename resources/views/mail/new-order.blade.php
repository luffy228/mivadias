<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                 <div class="card-header">{{ __('auth.hello') }}!</div>
                   <div class="card-body">
                         <div class="alert alert-success" role="alert">
                            {{ __('auth.ordersend') }}
                        </div>
                        {!! $content !!}
                </div>
            </div>
        </div>
    </div>
</div>