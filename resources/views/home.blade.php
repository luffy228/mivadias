@extends('layouts.app')

@section('home')
<div id="home" class="header-hero bg_cover" style="background-image: url(assets/images/banner-bg.svg)">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="header-hero-content text-center">
                    <h3 class="header-sub-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">Mivadias</h3>
                    <h2 class="header-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.5s">La plate-forme qui vous permet de payer vos factures du TOGO depuis l’ étranger</h2>
                </div> <!-- header hero content -->
            </div>
        </div> <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="header-hero-image text-center wow fadeIn" data-wow-duration="1.3s" data-wow-delay="1.4s">
                    <img src="{{asset('assets/images/header-hero.jpg')}}" alt="hero">
                </div> <!-- header hero image -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
    <div id="particles-1" class="particles"></div>
</div>
@endsection
@section('content')

<style>
    .imgCard {
        height: 360px;
    }

</style>




<section id="features" class="services-area pt-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title text-center pb-40">
                    <div class="line m-auto"></div>
                    <h3 class="title">Payer facilement et rapidement vos factures du Togo , <span> partout dans le monde</span></h3>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
        <div class="row justify-content-center">
            @foreach ($services_2_cols as $services)
            @foreach ($services as $service)
            <div class="col-lg-4 col-md-7 col-sm-8">
                 <!-- single services -->

               

                <div class="single-services text-center mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="services-icon">
                        <img class="shape" src="assets/images/services-shape.svg" alt="shape">
                        <img class="shape-1" src="{{asset($service->img_url) }}" alt="{{ $service->title }}">
                        
                    </div>
                    <div class="services-content mt-30">
                        <h4 class="services-title"><a href="/formulaire/{{ $service->id }}">{{ $service->title }}</a></h4>
                        <a class="more" href="/formulaire/{{ $service->id }}">Payer <i class="lni-chevron-right"></i></a>
                    </div>
                </div>
           
        
            </div>

            @endforeach            
            @endforeach            

        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== SERVICES PART ENDS ======-->

<!--====== ABOUT PART START ======-->

<section id="about" class="about-area pt-70">

    
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <div class="line"></div>
                        <h3 class="title">Rapide & Facile d'utilisation <span></span></h3>
                    </div> <!-- section title -->
                    <p class="text">Pour payer une facture depuis l' extérieur il suffit de créer un compte se connecter et ensuite choisir la facture que vous souhaitez payer et lancer le paiement.<br>
                        Vous recevrez un e-mail de confirmation de commande et lorsque la facture sera réglée vous serez notifier via e-mail.</p>
                    
                </div> <!-- about content -->
            </div>
            <div class="col-lg-6">
                <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/Mivadias.jpg" alt="about">
                </div> <!-- about image -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
    <div class="about-shape-1">
        <img src="assets/images/about-shape-1.svg" alt="shape">
    </div>
</section>
@stop
