@extends('layouts.app')

@section('content')
<div class="container">
<h1 style="margin-top: 35px;">Mentions légales</h1>
<p>Le site wWw.mivadias.com ainsi que tout son contenu et ses composants sont la propriété de E-SOLUX GROUP qui  est une société anonyme au capital de 1.000.000 FCFA dont le Siège social se situe au : 1 rue Léo 2000, à Agoè Lomé Togo .<br/><br/>
Téléphone : 00228 90 44 65 65<br/>
RCCM : TG-LFW-01-2021-B13-00777<br/>          
CNSS : 137915
</p>
</div>

@stop
