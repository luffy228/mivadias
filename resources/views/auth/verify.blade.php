@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifier votre adresse email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un lien vous a ete envoyer dans votre adresse email.') }}
                        </div>
                    @endif

                    {{ __('Avant de continuer verifier le lien qui vous a ete envoyer a travers votre email.') }}
                    {{ __('Si vous n avez pas recu d email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Valider') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
