<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Payer vos factures</title>
        <link rel="stylesheet" href="{{asset('css/sb-admin-2.css')}}">

    </head>
    <body>