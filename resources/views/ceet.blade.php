@extends('main')

@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Payer Facture CEET</h1>
    </div>
    <section>
        <div class="col-xl-12 col-lg-12">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                        <p> Entrer vos informations </p>
                           
                    </div>
                </div>
                <!-- Card Body -->
                <div class='ml-3 col-xl-4 col-lg-4'>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type='email' class='form-control' id='email' name='email'/>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type='text' class='form-control' id='phone' name='phone'/>
                    </div>

                    <div class="form-group">
                        <label for="montant">Montant</label>
                        <input type='number' class='form-control' id='montant' name='montant'/>
                    </div>

                    <div class="form-group">
                        <label for="fee">Frais</label>
                        <input type='number' class='form-control' id='fee' name='fee' disabled='disabled'/>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@stop
