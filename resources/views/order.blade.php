@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $title }}</h1>
    </div>
    <section>
        <div class="col-xl-12 col-lg-12">
        
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-edit text-white"></i>
                        </div>
                    </div>
                    <div class="d-inline-flex p-2">
                        <p> Detail De la commande </p>
                           
                    </div>
                </div>
                <!-- Card Body -->
                <div class='ml-3 col-xl-4 col-lg-4 pt-3 pb-3'>
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </section>
</div>


@stop
