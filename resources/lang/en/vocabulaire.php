<?php

return [

/*
|--------------------------------------------------------------------------
| Vocab Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

'login_btn' => 'Login',
'register_btn' => 'Register',
'logout_btn' => 'Logout',
'dashboard_btn' => 'Dashboard',
'customers_btn' => 'Customers',
'main_users_btn' => 'Main Users',
'orders_btn' => 'Orders',
'services_btn' => 'Services',
'pubs_btn' => 'Pubs',
'cancel_btn' => 'Cancel',
'email_addres_btn'  => 'E-Mail Address',
'password_btn'  => 'Password',
'remember_me_btn'  => 'Remember Me',
'forgot_password_btn' => 'Forgot Your Password?',
'name_btn' => 'Name',
'confirm_Password_btn' => 'Confirm Password',
'Reset_Password_btn'  => 'Reset Password',



];