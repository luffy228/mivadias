<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe a été réinitialisé',
    'sent' => 'Votre mal de reinitialisation de mot de passe a été envoyé!',
    'throttled' => 'Attendez avant de reesayer SVP.',
    'token' => 'La clé de reinitialisation de mot de passe et Invalide.',
    'user' => "Nous ne trouvons pas l'utilisateur correspondant a cette addresse mail.",

];
