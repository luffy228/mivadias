<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'L\'identifiant ou mot de passe erronée .',
    'Logout' => 'Se deconnecter?',
    'throttle' => 'Trop de connexion en même temps. Reessayer dans une seconde.',
    'ordersend' => 'Votre commande a été prise en compte et en attente de Traitement.',
    'orderconf' => 'Votre commande a été traité par un administrateur.',
    'hello' => 'Bonjour',

];
