<?php

return [

/*
|--------------------------------------------------------------------------
| Vocab Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

'login_btn' => 'Se connecter',
'register_btn' => 'S\'enregister',
'logout_btn' => 'Se déconnecter',
'dashboard_btn' => 'Tableau de bord?',
'customers_btn' => 'Clients',
'main_users_btn' => 'Gerances',
'orders_btn' => 'Commande(s)',
'services_btn' => 'Services',
'pubs_btn' => 'Publicités',
'cancel_btn' => 'Annuler',
'email_addres_btn'  => 'Adresse e-mail',
'password_btn'  => 'Mot de passe',
'remember_me_btn'  => 'Souviens-toi de moi',
'forgot_password_btn' => 'Mot de passe oublié?',
'name_btn' => 'Nom',
'confirm_Password_btn' => 'Confirmez le mot de passe',
'Reset_Password_btn'  => 'réinitialiser le mot de passe',


];