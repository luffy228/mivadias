<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('service_id')->foreign('service_id','services');
            $table->biginteger('user_id')->foreign('user_id','users');
            $table->date('dispatch_date');
            $table->string('status');
            $table->double('montant');
            $table->double('frais');
            $table->double('montant_total');
            $table->biginteger('user_validate_id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
