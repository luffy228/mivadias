<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFielsToFormulaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formulaires', function (Blueprint $table) {
            $table->string("libelle");
            $table->string("type");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formulaires', function (Blueprint $table) {
            $table->dropColumn('libelle');
            $table->dropColumn('type');
        });
    }
}
