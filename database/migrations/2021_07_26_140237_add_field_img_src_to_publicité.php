<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldImgSrcToPublicité extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publicites', function (Blueprint $table) {
            $table->string('img_src');
            $table->string('description')->nullable;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publicites', function (Blueprint $table) {
            $table->dropColumn('img_src');
            $table->dropColumn('description');
        });
    }
}
