<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services;
use App\User;

class Orders extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $incrementing = true;

    public function getServiceTitle(){
        $service = Services::find($this->service_id);
        return $service['title'];
    }

    public function getUsersName(){
        $users = User::find($this->user_id);
        //return $users->name;
        //$data =  $users['name'];
        if(isset($users->name)){
            return $users->name;
        }
  
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function service() {
        return $this->belongsTo('App\Services');
    }

    public function getDateValidation(){
        $date_val = $this->dispatch_date;
        if($date_val == '1970-01-01')
            $date_val = '-';
        return $date_val;
    }
}
