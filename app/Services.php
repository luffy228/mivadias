<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';
    protected $primaryKey = 'id';
    public $incrementing = true;

}
