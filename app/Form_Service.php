<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form_Service extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_service';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $incrementing = true;
}
