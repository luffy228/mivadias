<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class FormulaireForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $this
            ->add('email', 'text')
            ->add('phone', 'text')
            ->add('montant', 'number');
    }
}
