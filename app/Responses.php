<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Form_Service;
class Responses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'responses';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $incrementing = true;

    public function formService() {
        return $this->belongsTo('App\Form_Service');
    }
}
