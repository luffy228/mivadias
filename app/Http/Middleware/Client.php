<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $id = Auth::id();
            $user = User::find($id);
            if($user->role =='GERANCE')
                return redirect()->route('gerance.index');
            if($user->role =='ADMIN')
                return redirect()->route('admin.index');

        }
        return $next($request);
    }
}
