<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use App\User;

class Gerance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $id = Auth::id();
            $user = User::find($id);
            if($user->role !='GERANCE' && $user->role !='ADMIN')
                return redirect()->route('login');
            if($user->role =='ADMIN')
                return redirect()->route('admin.index');

        }else{
            return redirect()->route('login');
        }
        return $next($request);
    }
}
