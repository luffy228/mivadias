<?php
   
namespace App\Http\Controllers;

use App\Orders;
use App\Responses;
use App\User;
use Exception;
use Illuminate\Http\Request;
Use Mail;
use Session;
use Stripe;
   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe(Request $request)
    {
        
        /*
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));          
          
            $session = \Stripe\Checkout\Session::create([
              'line_items' => [[
                'price_data' => [
                  'currency' => 'eur',
                  'product_data' => [
                    'name' => $request["description"],
                  ],
                  'unit_amount' => $request["montant"],
                ],
                'quantity' => 1,
              ]],
              'mode' => 'payment',
              //'success_url' => 'https://example.com/success',
              //'cancel_url' => 'https://example.com/cancel',
            ]);

        */
        $montant = round($request["montant"],0) ;
        $description = $request["description"];
        $idorder = $request["idorder"];
        return view('stripe', compact('montant', 'description','idorder'));
        //return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        
        

        /*
        
        dd($request["etat"]);

        if ($request["etat"] == "success") {

            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $request["montant"],
                "currency" => "eur",
                "source" => $request->stripeToken,
                "description" => $request["description"] 
        ]);
        
            
        }
        else{
            
            $commentaire = "Echec";
            $html = "Erreur sur le payment de commande <a href='".route('order',['id'=>$idorder])."'>Voir la commande</a><br/>
             ";

             $user = User::find($order->user_id);

             $reponses = Responses::where('order_id', '=', $idorder)
                                 ->whereHas('formService', function ($query) use ($idorder) {
                                     $query->where('formulaire_id', '=', 3);
                               
                               
                             })
                             ->get();

             foreach ($reponses as $reponse ) {
                 $inputUser_mail = $reponse->value;
             }

             $data = [
                 'subject' => 'Commande N°'.$idorder,
                 'email' => [$user->email,$inputUser_mail],
                 //'email' => $user->email,
                 //'email' => "administration@azazel-inc.net",
                 'content' => $html
             ];
     
             Mail::send('mail.new-order', $data, function($message) use ($data) {
                 $message->bcc($data['email'])
                 ->subject($data['subject']);
             });

           return redirect()->route('orders');
        }
        
      
        */

        
        
        $idorder = $request["idorder"];
        $order = Orders::find($idorder);

        $commentaire = "Payé";
        $order->status = 'Payé';

        $html = "Commande payé avec success <a href='".route('order',['id'=>$idorder])."'>Voir la commande</a><br/>
         ";

         $user = User::find($order->user_id);


          $reponses = Responses::where('order_id', '=', $idorder)
                             ->whereHas('formService', function ($query) use ($idorder) {
                                 $query->where('formulaire_id', '=', 3);
                           
                           
                         })
                         ->get();

         foreach ($reponses as $reponse ) {
             $inputUser_mail = $reponse->value;
         }

         $data = [
             'subject' => 'Commande N°'.$idorder,
             'email' => [$user->email,$inputUser_mail,env('APP_ADM_MAIL',"mivadiastg@gmail.com")],
             //'email' => $user->email,
             //'email' => "administration@azazel-inc.net",
             'content' => $html
         ];
 
         Mail::send('mail.new-order', $data, function($message) use ($data) {
             $message->bcc($data['email'])
             ->subject($data['subject']);
         });
     
      // return redirect()->route('orders');

        
        
                 
        //return redirect()->to('/');
    }
}