<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\FormulaireForm;
use App\Services;
use App\Orders;
use App\TypeService;
use App\Responses;
use App\Formulaire;
use App\Form_Service;
use App\User;
use Kris\LaravelFormBuilder\Field;
use Illuminate\Support\Facades\Auth;
use Paydunya\Setup;
use Paydunya\Checkout\Store;
use Paydunya\Checkout\CheckoutInvoice;
use AmrShawky\LaravelCurrency\Facade\Currency;
use CinetPay\CinetPay;

Use Mail;

class PaymentController extends Controller
{
    private $formBuilder;

    public function __construct(FormBuilder $formBuilder)
    {
        $this->middleware('client');
        $this->formBuilder = $formBuilder;
        Setup::setMasterKey(env('MASTER_KEY',"wQzk9ZwR-Qq9m-0hD0-zpud-je5coGC3FHKW"));
        Setup::setPublicKey(env('PUBLIC_KEY',"test_public_kb9Wo0Qpn8vNWMvMZOwwpvuTUja"));
        Setup::setPrivateKey(env('PRIVATE_KEY',"test_private_rMIdJM3PLLhLjyArx9tF3VURAF5"));
        Setup::setToken(env('TOKEN_VAL',"IivOiOxGJuWhc5znlIiK"));
        Setup::setMode(env('MODE_PAY',"test"));

        Store::setName("Esolux facturation"); 

    }
    /**
     * Load Formulaire.
     *
     * @return Response
     */
    public function showFormulaire($id)
    {
        $service = Services::find($id);
        $form = $this->getForm($id);

        
        return view('formulaire', ['form'=>$form,'title'=>$service->title]);
    }
    
    public function currency($montant)
    {
      $return = array();
      $frais_total = ($montant * 10)/100;
      $return['data_one'] = $montant;  
      if($frais_total != null){
            $return['data_two'] = $frais_total;
      }
      
      $montant_total = $frais_total + $montant;
       if($montant_total != null){
            $return['data_three'] = $montant_total;
            $eur =  Currency::convert()
                      ->from('XOF')
                      ->to('EUR')
                      ->amount($montant_total)
                      ->get();
            $usd =  Currency::convert()
                      ->from('XOF')
                      ->to('USD')
                      ->amount($montant_total)
                      ->get();
                      $retJson = [
                        "EUR" => $eur,
                        "USD" => $usd
                      ];
            $return['data_euro'] = $eur;
            $return['data_usd'] = $usd;
        }
        echo json_encode($return);exit;
    }

    public function choixAmount($id, $quantity){
        $type_service = TypeService::find($id);
        $return = array();

        $prix_unitaire = $type_service->prix_unitaire;
        $amount_total = $prix_unitaire * $quantity;
         if($amount_total != null){
            $return['data_one'] = $amount_total;   
        }

        $service = Services::find($id);
        //$frais_total = ($amount_total * $service->fee)/100;
        $frais_total = ($amount_total * 10)/100;
        if($frais_total != null){
            $return['data_two'] = $frais_total;
        }

        $montant_total = $frais_total + $amount_total;
        if($montant_total != null){
            $return['data_three'] = $montant_total;
            $eur =  Currency::convert()
                      ->from('XOF')
                      ->to('EUR')
                      ->amount($montant_total)
                      ->get();
            $usd =  Currency::convert()
                      ->from('XOF')
                      ->to('USD')
                      ->amount($montant_total)
                      ->get();
                      $retJson = [
                        "EUR" => $eur,
                        "USD" => $usd
                      ];
            $return['data_euro'] = $eur;
            $return['data_usd'] = $usd;
        }
        
        

         echo json_encode($return);
    }

    /**
     * Make Payement.
     *
     * @return Response
     */
    public function MakePayement(Request $request)
    {
       
        
        $idOrder = $request->session()->get('ToDoOrder');
        $request->session()->forget('ToDoOrder');
        $order = Orders::find($idOrder);
        Store::setCallbackUrl(route('payement.callback',['idorder'=>$idOrder]));

       // echo route('payement.callback',['idorder'=>$idOrder]);exit;
        $service = Services::find($order->service_id);

        $invoice = new CheckoutInvoice();
        $invoice->addItem($service->title, $order->quantity, $order->montant, $order->montant);
        $invoice->setTotalAmount($order->montant_total);
        $invoice->addTax("Frais de service", $order->frais);
        $invoice->addCustomData("Commande", $idOrder);

       // print_r($service);exit;

       //return redirect()->route('payement.callback',['idorder'=>$idOrder]);
       $invoice->setCallbackUrl(route('payement.callback',['idorder'=>$idOrder]));
        $ret_text = "";
        if($invoice->create()) {
            header("Location: ".$invoice->getInvoiceUrl());
        }else{
            $ret_text = $invoice->response_text;
        }

        return view('payment', ['ret_text'=>$ret_text,'title'=>$service->title]);


    }
    public function CancelPay($idorder)
    {
        $order = Orders::find($idorder);

       $html = "Payement annulé <a href='".route('order',['id'=>$idorder])."'>Voir la commande</a><br/>
        ";

        $user = User::find($order->user_id);


        $reponses = Responses::where('order_id', '=', $idorder)
                                ->whereHas('formService', function ($query) use ($idorder) {
                                    $query->where('formulaire_id', '=', 3);
                              
                              
                            })
                            ->get();

        foreach ($reponses as $reponse ) {
            $inputUser_mail = $reponse->value;
        }

        $data = [
            'subject' => 'Commande N°'.$idorder,
            'email' => [$user->email, $inputUser_mail],
            //'email' => $user->email,
            //'email' => "administration@azazel-inc.net",
            'content' => $html
        ];

        Mail::send('mail.new-order', $data, function($message) use ($data) {
            $message->bcc($data['email'])
            ->subject($data['subject']);
        });

        return redirect()->route('home.index');


    }

    /**
     * Callback Payement.
     *
     * @return Response
     */
   public function CallBackPayement(Request $request,$idorder){

        $order = Orders::find($idorder);
        $_POST = $request->all();
        $commentaire = "";

        if (isset($_POST['cpm_trans_id'])){
            $id_transaction = $_POST['cpm_trans_id'];
            $apiKey = env('API_KEY_CINETEPAY');
            $site_id = env('SITE_ID');
            $plateform = env('PLATEFORM');
            $version =  env('VERSION');
            $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
            // Reprise exacte des bonnes données chez CinetPay
            $CinetPay->setTransId($id_transaction)->getPayStatus();
            $cpm_site_id = $CinetPay->_cpm_site_id;
            $signature = $CinetPay->_signature;
            $cpm_amount = $CinetPay->_cpm_amount;
            $cpm_trans_id = $CinetPay->_cpm_trans_id;
            $cpm_custom = $CinetPay->_cpm_custom;
            $cpm_currency = $CinetPay->_cpm_currency;
            $cpm_payid = $CinetPay->_cpm_payid;
            $cpm_payment_date = $CinetPay->_cpm_payment_date;
            $cpm_payment_time = $CinetPay->_cpm_payment_time;
            $cpm_error_message = $CinetPay->_cpm_error_message;
            $payment_method = $CinetPay->_payment_method;
            $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
            $cel_phone_num = $CinetPay->_cel_phone_num;
            $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
            $created_at = $CinetPay->_created_at;
            $updated_at = $CinetPay->_updated_at;
            $cpm_result = $CinetPay->_cpm_result;
            $cpm_trans_status = $CinetPay->_cpm_trans_status;
            $cpm_designation = $CinetPay->_cpm_designation;
            $buyer_name = $CinetPay->_buyer_name;
            if ($cpm_result == '00') {

               $commentaire = "Payé";
               $order->status = 'Payé';

               $html = "Commande payé avec success <a href='".route('order',['id'=>$idorder])."'>Voir la commande</a><br/>
                ";

                $user = User::find($order->user_id);


                 $reponses = Responses::where('order_id', '=', $idorder)
                                    ->whereHas('formService', function ($query) use ($idorder) {
                                        $query->where('formulaire_id', '=', 3);
                                  
                                  
                                })
                                ->get();

                foreach ($reponses as $reponse ) {
                    $inputUser_mail = $reponse->value;
                }

                $data = [
                    'subject' => 'Commande N°'.$idorder,
                    'email' => [$user->email,$inputUser_mail,env('APP_ADM_MAIL',"mivadiastg@gmail.com")],
                    //'email' => $user->email,
                    //'email' => "administration@azazel-inc.net",
                    'content' => $html
                ];
        
                Mail::send('mail.new-order', $data, function($message) use ($data) {
                    $message->bcc($data['email'])
                    ->subject($data['subject']);
                });
            
              return redirect()->route('orders');
               
            } else {

               $commentaire = "Echec";
               $html = "Erreur sur le payment de commande <a href='".route('order',['id'=>$idorder])."'>Voir la commande</a><br/>
                ";

                $user = User::find($order->user_id);

                $reponses = Responses::where('order_id', '=', $idorder)
                                    ->whereHas('formService', function ($query) use ($idorder) {
                                        $query->where('formulaire_id', '=', 3);
                                  
                                  
                                })
                                ->get();

                foreach ($reponses as $reponse ) {
                    $inputUser_mail = $reponse->value;
                }

                $data = [
                    'subject' => 'Commande N°'.$idorder,
                    'email' => [$user->email,$inputUser_mail],
                    //'email' => $user->email,
                    //'email' => "administration@azazel-inc.net",
                    'content' => $html
                ];
        
                Mail::send('mail.new-order', $data, function($message) use ($data) {
                    $message->bcc($data['email'])
                    ->subject($data['subject']);
                });

              return redirect()->route('orders');
            }

            $order->commentaire = $commentaire;

            $order->save();
            echo  $order->commentaire;exit;
        }
        else {
            
            header('Location: /');
            die();
        }
    }

    public function MakePayementCinetPay(Request $request){

        $idOrder = $request->session()->get('ToDoOrder');
        $request->session()->forget('ToDoOrder');
        $order = Orders::find($idOrder);
        $service_id = ($order['service_id']) ;
        
        $service = Services::find($service_id);
        
     
        $id_transaction = $idOrder."-".$service['title'];
        $description_du_paiement = "Mon produit de ref: $id_transaction";
        $date_transaction = date("Y-m-d H:i:s");
        $montant_a_payer = $order['montant_total'];
        $identifiant_du_payeur = $idOrder ;
        $devise = 'XOF';
        $apiKey = env('API_KEY_CINETEPAY');
        $site_id = env('SITE_ID');
        $plateform = env('PLATEFORM');
        $version = env('VERSION');
        $formName = "Payer avec mobile Money";
        $notify_url = '';
        $return_url = route('payement.callback',['idorder'=>$idOrder]);
        $cancel_url = route('payement.cancel',['idorder'=>$idOrder]);
        $btnType = 1;
        $btnSize = 'large';
      

            $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
               $bnt_pay =  $CinetPay->setTransId($id_transaction)
                ->setDesignation($description_du_paiement)
                ->setTransDate($date_transaction)
                ->setAmount($montant_a_payer)
                ->setCurrency($devise)
                ->setDebug(false)// put it on true, if you want to activate debug
                ->setCustom($identifiant_du_payeur)// optional
                ->setNotifyUrl($notify_url)// optional
                ->setReturnUrl($return_url)// optional
                ->setCancelUrl($cancel_url)// optional
                ->getPayButton($formName, $btnType, $btnSize);

                
           // ->displayPayButton();
        $order_serv = Orders::findOrFail($idOrder);
        $form = $this->getFormValService($order_serv->service_id,$idOrder,false);
        $montant_euro = Currency::convert()
                      ->from('XOF')
                      ->to('EUR')
                      ->amount($montant_a_payer)
                      ->get();

        return view('checkout', ['prix_total'=>$montant_a_payer,'devise'=>$devise,'title'=>$service->title,'bnt_pay'=>$bnt_pay,'form'=>$form , 'description'=> $description_du_paiement , 'prix_euro'=>$montant_euro , 'idorder'=>$idOrder]);

    }

    private function getFormValService($service_id,$id,$show_btn=true)
    {
        //recuperation formulaire

        $formulaires_service = Form_Service::where('service_id', '=', $service_id)->get();
        $responses = Responses::where('service_id', '=', $service_id)
                                ->where('order_id', '=', $id)
                                ->get();

        $res_array = [];
        foreach ($responses as $response) {
            $res_array += [$response->form_service_id => $response->value];
        }
        $options = array();
        $montant_default = 0;
        foreach ($formulaires_service as  $form_service) {
            $formulaire = Formulaire::find($form_service->formulaire_id);
            $count = TypeService::where('form_serv_id', '=', $form_service->id)->count();
            
            if($count>0)
            {
                $type_services = TypeService::where('form_serv_id', '=', $form_service->id)->get();
                $choice = [];

                foreach ($type_services as $key => $type_service) {
                    $choice += [ $type_service->id => $type_service->libelle ];
                    
                }

                $montant_default = $type_services[0]->prix_unitaire;
                //print_r($choice);

                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'type' => "text",
                    'value' => $choice[$res_array[$form_service->id]]
                ];
            }else{
                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'value' => $res_array[$form_service->id],
                    'type' => $formulaire->type
                ];
            }
            

            
            $options[]=$opt;
        }

        $opt_montant = [
            'name' => "montant",
            'type' => "number",
            'value' => $montant_default
        ];

      
        if($show_btn)
            $options[]=$opt_montant;

        $order = Orders::find($id);
        $opt_frais = [
            'name' => "frais",
            'label' => "Frais de service",
            'type' => "number",
            'value' => $order->frais
        ];
        $options[]=$opt_frais;

        $opt_montant_total = [
            'name' => "mt",
            'label' => "Montant total",
            'type' => "number",
            'value' => $order->montant_total
        ];
        $options[]=$opt_montant_total;

       

        
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('admin.validateorder',['id'=>$id])
            ]);

        return $form;
    }
  

    function loadtestPost(){
       
        return  array(
          'code' => 0,
          'message' => 'OPERATION_SUCCES',
          'data' => 
            array (
             'transaction_id' => '00',
             'client_transaction_id' => 'Transaction Found',
             'lot' =>  '0044557641201530102473',
             'amount' => '500',
             'receiver' => '07895086',
             'receiver_e164'=> '+22507895086',
             'operator'=> 'OM',
             'sending_status'=> 'CONFIRM',
             'transfer_valid'=> 'Y',
             'treatment_status'=> 'VAL',
             'comment'=> 'Transfert effectué avec succès',
             'validated_at' => '2018-06-27 12:53:26'
                  
                ) 
        );
    }

    

    public function store(Request $request,$id)
    {
        //$form = $this->getForm($id);
        //$model = $form->getModel();
         $validated = $request->validate([
            'name' => 'required',
            'reference' => 'required',
            'email' => 'required',
        ]);
        if ($_POST['montant'] < 1000) {
            return back();
        }


        $orderid = $this->saveOrderByPost($_POST,$id);
        session(['ToDoOrder' => $orderid ]);
        /*$form->redirectIfNotValid();
        $form->getModel()->save();*/
        if (Auth::check()) {
            $id = Auth::id();
            $order = Orders::find($orderid);
            $order->user_id = $id;
            $order->commentaire = "Payement en attente";
            $order->save();

            $html = "Payement en attente  <a href='".route('order',['id'=>$orderid])."'>Voir la commande</a><br/>
                    ";

                    $user = User::find($order->user_id);

                     $reponses = Responses::where('order_id', '=', $orderid)
                                        ->whereHas('formService', function ($query) use ($orderid) {
                                            $query->where('formulaire_id', '=', 3);
                                      
                                      
                                    })
                                    ->get();

                        foreach ($reponses as $reponse ) {
                            $inputUser_mail = $reponse->value;
                        }

                    $data = [
                        'subject' => 'Commande N°'.$orderid,
                        'email' => [$user->email,$inputUser_mail],
                        //'email' => "thesita97@gmail.com",
                        'content' => $html
                    ];
            
                    /*
                    Mail::send('mail.new-order', $data, function($message) use ($data) {
                        $message->to($data['email'])
                        ->subject($data['subject']);
                    });*/
                
            return redirect()->route('payement.pay');
        }else{
            return redirect()->route('loginspecial');
        }

        
    }

    function saveOrderByPost($post,$id){
        $service = Services::find($id);
        $order = new Orders();
        $order->service_id = $id;
        $order->user_id = 0;
        $order->dispatch_date = '1970-01-01 00:00:00';
        $order->status = 'ToDo';
        $order->montant = $post['montant'];
        $order->user_validate_id  = 0;
        $order->frais = ($post['montant']*$service->fee)/100;
        $order->montant_total  = (($post['montant']*$service->fee)/100)+$post['montant'];
        $order->quantity = 1;
        if(isset($post['duree']))
        {
            $order->quantity = $post['duree'];
            $order->frais = ($post['montant']*$order->quantity * $service->fee)/100;
            $order->montant_total  = $order->frais + $post['montant'] * $order->quantity;
        }
            
        $order->save();

        $orderId = $order->id;

        $formulaires_service = Form_Service::where('service_id', '=', $id)->get();
        foreach ($formulaires_service as  $form_service) {
            $formulaire = Formulaire::find($form_service->formulaire_id);

            $response = new Responses();

            $response->service_id = $id;
            $response->form_service_id = $form_service->id;
            $response->value  = $post[$formulaire->libelle];
            $response->order_id  = $orderId;
            $response->save();

        }

        

        return $orderId;


    }

    private function getForm($service_id)
    {
        //recuperation formulaire

        $formulaires_service = Form_Service::where('service_id', '=', $service_id)->get();
        $options = array();
        $montant_default = 0;
        $frais_default = 0;
        $MontantTotal_default = 0;
        foreach ($formulaires_service as  $form_service) {
            $formulaire = Formulaire::find($form_service->formulaire_id);
            $count = TypeService::where('form_serv_id', '=', $form_service->id)->count();
            $service = Services::find($form_service->service_id);
            $order = new Orders();
            $order->user_id = 0;
               

            if($count>0)
            {
                $type_services = TypeService::where('form_serv_id', '=', $form_service->id)->get();
                $choice = [];

                foreach ($type_services as $key => $type_service) {
                    $choice += [ $type_service->id => $type_service->libelle ];
                    
                }
                $montant_default = $type_services[0]->prix_unitaire;
                $frais_default = ($montant_default*$service->fee)/100;
                $MontantTotal_default =  $frais_default +  $montant_default; 
                
                //print_r($choice);
            

                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'type' => $formulaire->type,
                    'choices' => $choice,
                    //'choices' => ['7' => 'English', '6' => 'French'],
                    'selected'=> $type_services[0]->id
                ];
            }else{
                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'value' => $form_service->default,
                    'type' => $formulaire->type
                ];
            }
            

            
            $options[]=$opt;
        }

        $opt_montant = [
            'name' => "montant",
            'label' => "montant(XOF) : minimum 1000XOF",
            'type' => "number",
            'value' => $montant_default
        ];

        $opt_btn = [
            'name' => "Suivant",
            'class' => "btn-primary",
            'type' => "submit"
        ];
     

        
            $options[]=$opt_montant;

            $opt_frais = [
                'name' => "frais",
                'type' => "number",
                'attr' => ['disabled' => 'disabled'],
                'value' => $frais_default
               ];
            $options[]=$opt_frais;

            $opt_montantTotal = [
                'name' => "montant_total",
                'label' => "Montant total(XOF)",
                'type' => "number",
                'attr' => ['disabled' => 'disabled'],
                'value' =>$MontantTotal_default
            ];
            $options[]=$opt_montantTotal;

            $opt_montantTotalEuro = [
                'name' => "montant_total_euro",
                'label' => "Montant total(Euro)",
                'type' => "number",
                'attr' => ['disabled' => 'disabled'],
                'value' =>""
            ];
            $options[]=$opt_montantTotalEuro;

            $opt_montantTotalDollar = [
                'name' => "montant_total_dollar",
                'label' => "Montant total(Dollar)",
                'type' => "number",
                'attr' => ['disabled' => 'disabled'],
                'value' =>""
            ];
            $options[]=$opt_montantTotalDollar;

        $options[]=$opt_btn;
        
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('payment.store',['id'=>$service_id])
            ]);
        
        return $form;
    }
}
