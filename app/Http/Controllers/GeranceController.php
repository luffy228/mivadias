<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Orders;
use App\Formulaire;
use App\TypeService;
use App\Form_Service;
use App\Responses;
use App\Services;
use App\Publicite;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;
use Mail;

class GeranceController extends Controller
{
    private $formBuilder;
    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }
    function index(){
        return view('gerance.index');
    }

    function orders(){
        $orders = Orders::where('status', '=', 'Payé')->paginate(15);

        return view('gerance.orders',[ "orders" => $orders, 'status' => "Payé", "valsearch" => '' ]);
    }

    function ordersby($ordre, $status, $input){

        $valsearch = "";
        if($action == "status"){
            if($status == "tous"){
                $orders = Orders::where($action, '=', "Payé")
                                ->orWhere(function($query){
                                    $query->where('status', '=', "Validée")
                                    ->where('user_validate_id', '=' , Auth::id());
                                })->paginate(15);

                if($value != "val"){
                    $orders = Orders::where(function($query){
                                $query->where('status', '=', "Payé")
                                        ->orWhere(function($query){
                                            $query->where('status', '=', "Validée")
                                            ->where('user_validate_id', '=' , Auth::id());
                                        });
                                })
                                ->where(function($query) use ($value){
                                    $query->where('created_at', 'like', '%'.$value.'%')
                                          ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                          ->orWhere('montant', 'like', '%'.$value.'%')
                                          ->orWhere('frais', 'like', '%'.$value.'%')
                                          ->orWhere('montant_total', 'like', '%'.$value.'%')
                                          ->orWhere('quantity', 'like', '%'.$value.'%')
                                          ->orWhereHas('user', function ($query) use ($value) {
                                                $query->where('name', 'like', '%'.$value.'%');
                                            })
                                          ->orWhereHas('service', function ($query) use ($value) {
                                                $query->where('title', 'like', '%'.$value.'%');
                                            });
                                })
                                ->paginate(15);
                    $valsearch = $value;
                }
            }else if($status == "Payé"){
                $orders = Orders::where($action, '=', $status)->paginate(15);
                if($value != "val"){
                    $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
                }
            }else if($status == "Validée"){
                $orders = Orders::where($action, '=', $status)->where('user_validate_id', '=' , Auth::id() )->paginate(15);
                if($value != "val"){
                    $orders = Orders::where('status', '=', $status)
                            ->where('user_validate_id', '=' , Auth::id() )
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
                }
            }
        }

        if($action == "search" && $value != "vide"){
            if($value != "val"){
                $orders = Orders::where(function($query){
                            $query->where('status', '=', "Payé")
                                    ->orWhere(function($query){
                                        $query->where('status', '=', "Validée")
                                        ->where('user_validate_id', '=' , Auth::id());
                                    });
                            })
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                $valsearch = $value;
            } else if($status == "Payé"){
                $orders = Orders::where('status', '=', $status)
                        ->where(function($query) use ($value){
                            $query->where('created_at', 'like', '%'.$value.'%')
                                  ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                  ->orWhere('montant', 'like', '%'.$value.'%')
                                  ->orWhere('frais', 'like', '%'.$value.'%')
                                  ->orWhere('montant_total', 'like', '%'.$value.'%')
                                  ->orWhere('quantity', 'like', '%'.$value.'%')
                                  ->orWhereHas('user', function ($query) use ($value) {
                                        $query->where('name', 'like', '%'.$value.'%');
                                    })
                                  ->orWhereHas('service', function ($query) use ($value) {
                                        $query->where('title', 'like', '%'.$value.'%');
                                    });
                        })
                        ->paginate(15);
                
                $valsearch = $value;
            } else if($status == "Validée"){
                $orders = Orders::where('status', '=', $status)
                            ->where('user_validate_id', '=' , Auth::id() )
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
            }
        }else if($action == "search"){
            if($status != "tous"){
                $orders = Orders::where('status', '=', $status)->paginate(15);
                if($status != "Payé")
                    $orders = Orders::where('status', '=', $status)->where('user_validate_id', '=' , Auth::id() )->paginate(15);
            }
            else{
                $orders = Orders::where('status', '=', "Payé")
                                ->orWhere(function($query){
                                    $query->where('status', '=', "Validée")
                                    ->where('user_validate_id', '=' , Auth::id());
                                })->paginate(15);
            }

            $valsearch = "";
        }


        return view('gerance.orders',[ "orders" => $orders, "status" => $status, "valsearch" => $valsearch ]);
    }

    function ordersOperation($action, $value, $status){

        $valsearch = "";
        if($action == "status"){
            if($status == "tous"){
                $orders = Orders::where($action, '=', "Payé")
                                ->orWhere(function($query){
                                    $query->where('status', '=', "Validée")
                                    ->where('user_id', '=' , Auth::id());
                                })->paginate(15);

                if($value != "val"){
                    $orders = Orders::where(function($query){
                                $query->where('status', '=', "Payé")
                                        ->orWhere(function($query){
                                            $query->where('status', '=', "Validée")
                                            ->where('user_id', '=' , Auth::id());
                                        });
                                })
                                ->where(function($query) use ($value){
                                    $query->where('created_at', 'like', '%'.$value.'%')
                                          ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                          ->orWhere('montant', 'like', '%'.$value.'%')
                                          ->orWhere('frais', 'like', '%'.$value.'%')
                                          ->orWhere('montant_total', 'like', '%'.$value.'%')
                                          ->orWhere('quantity', 'like', '%'.$value.'%')
                                          ->orWhereHas('user', function ($query) use ($value) {
                                                $query->where('name', 'like', '%'.$value.'%');
                                            })
                                          ->orWhereHas('service', function ($query) use ($value) {
                                                $query->where('title', 'like', '%'.$value.'%');
                                            });
                                })
                                ->paginate(15);
                    $valsearch = $value;
                }
            }
            else if($status == "Payé"){
                $orders = Orders::where($action, '=', $status)->paginate(15);
                if($value != "val"){
                    $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
                }
            }else if($status == "Validée"){
                $orders = Orders::where($action, '=', $status)->where('user_id', '=' , Auth::id() )->paginate(15);
                if($value != "val"){
                    $orders = Orders::where('status', '=', $status)
                            ->where('user_id', '=' , Auth::id() )
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
                }
            }
        }

        if($action == "search" && $value != "vide"){
            if($value != "val"){
                $orders = Orders::where(function($query){
                            $query->where('status', '=', "Payé")
                                    ->orWhere(function($query){
                                        $query->where('status', '=', "Validée")
                                        ->where('user_id', '=' , Auth::id());
                                    });
                            })
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                $valsearch = $value;
            } else if($status == "Payé"){
                $orders = Orders::where('status', '=', $status)
                        ->where(function($query) use ($value){
                            $query->where('created_at', 'like', '%'.$value.'%')
                                  ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                  ->orWhere('montant', 'like', '%'.$value.'%')
                                  ->orWhere('frais', 'like', '%'.$value.'%')
                                  ->orWhere('montant_total', 'like', '%'.$value.'%')
                                  ->orWhere('quantity', 'like', '%'.$value.'%')
                                  ->orWhereHas('user', function ($query) use ($value) {
                                        $query->where('name', 'like', '%'.$value.'%');
                                    })
                                  ->orWhereHas('service', function ($query) use ($value) {
                                        $query->where('title', 'like', '%'.$value.'%');
                                    });
                        })
                        ->paginate(15);
                
                $valsearch = $value;
            } else if($status == "Validée"){
                $orders = Orders::where('status', '=', $status)
                            ->where('user_id', '=' , Auth::id() )
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    
                    $valsearch = $value;
            }
        }else if($action == "search"){
            if($status != "tous"){
                $orders = Orders::where('status', '=', $status)->paginate(15);
                if($status != "Payé")
                    $orders = Orders::where('status', '=', $status)->where('user_validate_id', '=' , Auth::id() )->paginate(15);
            }
            else{
                $orders = Orders::where('status', '=', "Payé")
                                ->orWhere(function($query){
                                    $query->where('status', '=', "Validée")
                                    ->where('user_id', '=' , Auth::id());
                                })->paginate(15);
            }

            $valsearch = "";
        }

        return view('gerance.orders',[ "orders" => $orders, "status" => $status, "valsearch" => $valsearch ]);
    }
   
    function validateorder($id){
        $order = Orders::find($id);
        $order->status = 'Validée';
        $order->user_validate_id = Auth::id();
        $order->dispatch_date = date('Y-m-d');
        $order->save();

        $html = "<a href='".route('order',['id'=>$id])."'>Voir la commande</a><br/><a href='".$order->url_fact."'>Telecharger la facture</a>";


        $reponses = Responses::where('order_id', '=', $id)
                                ->whereHas('formService', function ($query) use ($id) {
                                    $query->where('formulaire_id', '=', 3);
                              
                              
                            })
                            ->get();

        foreach ($reponses as $reponse ) {
            $inputUser_mail = $reponse->value;
        }

        $user = User::find($order->user_id);

        $data = [
            'subject' => 'Confirmation commande Commande N°'.$id,
            'email' => [$user->email, $inputUser_mail],
            'content' => $html
          ];
  
          Mail::send('mail.conf-order', $data, function($message) use ($data) {
            $message->to($data['email'])
            ->subject($data['subject']);
          });
        return redirect()->route('gerance.orders');
    }

    function order($id){
        $order = Orders::find($id);

        $form = $this->getFormValService($order->service_id,$id);

        return view('gerance.order',[ "order" => $order ,'form'=>$form,'title' => $order->getServiceTitle() ]);
    }

    private function getFormValService($service_id,$id)
    {
        //recuperation formulaire

        $formulaires_service = Form_Service::where('service_id', '=', $service_id)->get();
        $responses = Responses::where('service_id', '=', $service_id)->get();

        $res_array = [];
        foreach ($responses as $response) {
            $res_array += [$response->form_service_id => $response->value];
        }
        $options = array();
        $montant_default = 0;
        foreach ($formulaires_service as  $form_service) {
            $formulaire = Formulaire::find($form_service->formulaire_id);
            $count = TypeService::where('form_serv_id', '=', $form_service->id)->count();
            
            if($count>0)
            {
                $type_services = TypeService::where('form_serv_id', '=', $form_service->id)->get();
                $choice = [];

                foreach ($type_services as $key => $type_service) {
                    $choice += [ $type_service->id => $type_service->libelle ];
                    
                }

                $montant_default = $type_services[0]->prix_unitaire;
                //print_r($choice);

                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'type' => "text",
                    'value' => $choice[$res_array[$form_service->id]]
                ];
            }else{
                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'value' => $res_array[$form_service->id],
                    'type' => $formulaire->type
                ];
            }
            

            
            $options[]=$opt;
        }

        $opt_montant = [
            'name' => "montant",
            'type' => "number",
            'value' => $montant_default
        ];

        

        $opt_btn = [
            'name' => "Confirmé",
            'class' => "btn-primary",
            'type' => "submit"
        ];

        $options[]=$opt_montant;

        $order = Orders::find($id);
        $opt_frais = [
            'name' => "frais",
            'label' => "Frais de service",
            'type' => "number",
            'value' => $order->frais
        ];
        $options[]=$opt_frais;

        $opt_montant_total = [
            'name' => "mt",
            'label' => "Montant total",
            'type' => "number",
            'value' => $order->montant_total
        ];
        $options[]=$opt_montant_total;

        if($order->status != 'Validée' && $order->status != 'Annulée' )
            $options[]=$opt_btn;

        
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('gerance.validateorder',['id'=>$id])
            ]);


            foreach ($form->getFields() as $field) {
                //if($field->type != 'submit')
                    $field->setOptions([
                        'attr' => ['disabled' => 'disabled']
                    ]);
            }
            if($order->status != 'Validée' && $order->status != 'Annulée' )
                $form->getField('Confirmé')->setOptions([
                    'attr' => ['disabled' => false]
                ]);
        
        return $form;
    }

   
}
