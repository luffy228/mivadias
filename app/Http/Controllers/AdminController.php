<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Orders;
use App\Formulaire;
use App\TypeService;
use App\Form_Service;
use App\Responses;
use App\Services;
use App\Publicite;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;
use Mail;

class AdminController extends Controller
{
    private $formBuilder;
    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }
    function index(){
        return view('admin.index');
    }

    function validateorder($id){
        $order = Orders::find($id);
        $order->status = 'Validée';
        $order->user_validate_id = Auth::id();
        $order->dispatch_date = date('Y-m-d');
        $order->save();

        $html = "<a href='".route('order',['id'=>$id])."'>Voir la commande</a><br/><a href='".$order->url_fact."'>Telecharger la facture</a>";

        $user = User::find($order->user_id);

        $data = [
            'subject' => 'Confirmation commande Commande N°'.$id,
            'email' => $user->email,
            'content' => $html
          ];
  
          Mail::send('mail.conf-order', $data, function($message) use ($data) {
            $message->to($data['email'])
            ->subject($data['subject']);
          });
        return redirect()->route('admin.orders');
    }

    function orders(){



        

        $orders = Orders::where('status', '=', 'Payé')->paginate(15);


        return view('admin.orders',[ "orders" => $orders, 'status' => "Payé", "valsearch" => '' ]);
    }



    function ordersby($ordre, $status, $input){
        if($status != "tous"){
            $orders = Orders::where('status', '=', $status)->orderBy('created_at', $ordre)->paginate(15);
            if($input != "vide"){
                $orders = Orders::where('status', '=', $status)
                                ->where(function($query) use ($input){
                                    $query->where('created_at', 'like', '%'.$input.'%')
                                          ->orWhere('dispatch_date', 'like', '%'.$input.'%')
                                          ->orWhere('montant', 'like', '%'.$input.'%')
                                          ->orWhere('frais', 'like', '%'.$input.'%')
                                          ->orWhere('montant_total', 'like', '%'.$input.'%')
                                          ->orWhere('quantity', 'like', '%'.$input.'%')
                                          ->orWhereHas('user', function ($query) use ($input) {
                                                $query->where('name', 'like', '%'.$input.'%');
                                            })
                                          ->orWhereHas('service', function ($query) use ($input) {
                                                $query->where('title', 'like', '%'.$input.'%');
                                            });
                                })
                                ->orderBy('created_at', $ordre)
                                ->paginate(15);
            }else
                $input = "";
        }else{
            $orders = Orders::orderBy('created_at', $ordre)->paginate(15);
            if($input != "vide"){
                $orders = Orders::where(function($query) use ($input){
                                    $query->where('created_at', 'like', '%'.$input.'%')
                                          ->orWhere('dispatch_date', 'like', '%'.$input.'%')
                                          ->orWhere('montant', 'like', '%'.$input.'%')
                                          ->orWhere('frais', 'like', '%'.$input.'%')
                                          ->orWhere('montant_total', 'like', '%'.$input.'%')
                                          ->orWhere('quantity', 'like', '%'.$input.'%')
                                          ->orWhereHas('user', function ($query) use ($input) {
                                                $query->where('name', 'like', '%'.$input.'%');
                                            })
                                          ->orWhereHas('service', function ($query) use ($input) {
                                                $query->where('title', 'like', '%'.$input.'%');
                                            });
                                })
                                ->orderBy('created_at', $ordre)
                                ->paginate(15);

            }else
                $input = "";
        }
        
        return view('admin.orders',[ "orders" => $orders, "status" => $status , "valsearch" => $input ]);
    }

    function ordersOperation($action, $value, $status){

        $valsearch = "";
        if($action == "status"){
            if($status != "tous"){
                $orders = Orders::where($action, '=', $status)->paginate(15);
                if($value != "val"){
                    
                    $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    $valsearch = $value;
                }
            } else {
                $orders = Orders::paginate(15);
                if($value != "val"){
                    $orders = Orders::where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
                    $valsearch = $value;
                }
            }

            $valstatus = $status;
        }

        if($action == "search" && $value != "vide"){
            $valsearch = $value;
            $orders = array();

            if($status != "tous"){
                $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
            }else{
                $orders = Orders::where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);
            }
            

        }else if($action != "status"){
            if($status != "tous")         
                $orders = Orders::where("status", '=', $status)->paginate(15);
            else
                $orders = Orders::paginate(15);

            $valsearch = "";
        }



        return view('admin.orders',[ "orders" => $orders, "status" => $status, "valsearch" => $valsearch ]);


    }

    function order($id){
        $order = Orders::findOrFail($id);
        $form = $this->getFormValService($order->service_id,$id);

        return view('admin.order',[ "order" => $order ,'form'=>$form,'title' => $order->getServiceTitle() ]);
    }

    function orderclient($id){
        $order = Orders::findOrFail($id);
        $form = $this->getFormValService($order->service_id,$id,false);

        return view('order',[ "order" => $order ,'form'=>$form,'title' => $order->getServiceTitle() ]);
    }

    function client(){
        $users = User::where('role', '=', null)->get();

        

        return view('admin.client',[ "users" => $users ]);
    }

    function geranceuser(){
        $users = User::where('role', '=', 'GERANCE')->get();
        $form = $this->getForm();

        

        return view('admin.geranceuser',[ "users" => $users ,'form'=>$form]);
    }
    private function getFormValService($service_id,$id,$show_btn=true)
    {
        //recuperation formulaire

        $formulaires_service = Form_Service::where('service_id', '=', $service_id)->get();
        $responses = Responses::where('service_id', '=', $service_id)->where('order_id', '=', $id)->get();

        $res_array = [];
        foreach ($responses as $response) {
            $res_array += [$response->form_service_id => $response->value];
        }
        $options = array();
        $montant_default = 0;
        foreach ($formulaires_service as  $form_service) {
            $formulaire = Formulaire::find($form_service->formulaire_id);
            $count = TypeService::where('form_serv_id', '=', $form_service->id)->count();
            
            if($count>0)
            {
                $type_services = TypeService::where('form_serv_id', '=', $form_service->id)->get();
                $choice = [];

                foreach ($type_services as $key => $type_service) {
                    $choice += [ $type_service->id => $type_service->libelle ];
                    
                }

                $montant_default = $type_services[0]->prix_unitaire;
                //print_r($choice);

                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'type' => "text",
                    'value' => $choice[$res_array[$form_service->id]]
                ];
            }else{
                $opt = [
                    'name' => $formulaire->libelle,
                    'label' => $form_service->intitule,
                    'value' => $res_array[$form_service->id],
                    'type' => $formulaire->type
                ];
            }
            

            
            $options[]=$opt;
        }

        $opt_montant = [
            'name' => "montant",
            'type' => "number",
            'value' => $montant_default
        ];

        

        $opt_btn = [
            'name' => "Confirmé",
            'class' => "btn-primary",
            'type' => "submit"
        ];
        if($show_btn)
            $options[]=$opt_montant;

        $order = Orders::find($id);
        $opt_frais = [
            'name' => "frais",
            'label' => "Frais de service",
            'type' => "number",
            'value' => $order->frais
        ];
        $options[]=$opt_frais;

        $opt_montant_total = [
            'name' => "mt",
            'label' => "Montant total",
            'type' => "number",
            'value' => $order->montant_total
        ];
        $options[]=$opt_montant_total;

        if($order->status != 'Validée' && $order->status != 'Annulée'  && $show_btn)
            $options[]=$opt_btn;

        
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('admin.validateorder',['id'=>$id])
            ]);


            foreach ($form->getFields() as $field) {
                //if($field->type != 'submit')
                    $field->setOptions([
                        'attr' => ['disabled' => 'disabled']
                    ]);
            }
            if($order->status != 'Validée' && $order->status != 'Annulée' && $show_btn)
                $form->getField('Confirmé')->setOptions([
                    'attr' => ['disabled' => false]
                ]);
        
        return $form;
    }

    private function getForm()
    {
        $opt = [
            'name' => "name",
            'label' => "Nom",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;
        $opt = [
            'name' => "email",
            'label' => "Email",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;

        $opt = [
            'name' => "password",
            'label' => "Mot de pass",
            'value' => "123456789",
            'type' => "password"
        ];
        $options[]=$opt;
        $opt_btn = [
            'name' => "Suivant",
            'class' => "btn-primary",
            'type' => "submit"
        ];

        $options[]=$opt_btn;
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('admin.newuser',['id',])
            ]);
        
        return $form;

    }

    function newuser(){
        $user = new User();
        $user->name = $_POST['name'];
        $user->email = $_POST['email'];
        $user->role = "GERANCE";
        $user->password = Hash::make($_POST['password']);

        $user->save();
        return redirect()->route('admin.geranceuser');
    }


    function services(){
         $services = Services::all();
         $formulaires = Formulaire::all();
            $detail_service = array();
            foreach ($services as $service) {

                $detail = array();
                $detail['title'] = $service->title;
                $detail['fee'] = $service->fee;
                $detail['idservice'] = $service->id;
                $detail['created_at'] = $service->created_at;
                $detail['img_url'] = $service->img_url;
                $detail['status'] = $service->status;

                $FormServices = Form_Service::where('service_id', '=', $service->id)->get();
                //print_r($FormServices);exit;
                $i = 0;
                foreach ($FormServices as $FormService) {
                    $detail_formService[$i]['intitule'] = $FormService->intitule;
                    $detail_formService[$i]['default'] = $FormService->default;
                    $detail_formService[$i]['formulaire_id'] = $FormService->formulaire_id;
                    //$formulaire = Formulaire::where('id', '=', $FormService->formulaire_id)->get();
                    $formulaire = Formulaire::findOrFail( $FormService->formulaire_id);
                    $detail_formService[$i]['libelle'] = $formulaire->libelle;
                    $detail_formService[$i]['type'] = $formulaire->type;
                    $i++;
                }
                $detail['formService'] = $detail_formService;

                $detail_service[] = $detail;

            }

       
       // print_r($detail_service);exit;

        return view('admin.services',[ "services" => $services,'detail_service'=> $detail_service,'formulaires'=> $formulaires ]);
    }

    function service($id){
        $service = Services::findOrFail($id);
        
    }

    function delservices($id){
        $service = Services::findOrFail($id);
        return redirect()->route('admin.services');
    }



    function modifservices(Request $request,$id){
       $service = Services::findOrFail($id);
    

        $service->title = $_POST['title'];
        $service->fee = $_POST['fee'];

        if ($request->hasFile('image')) {
     
            if ($request->file('image')->isValid()) {
             
                $validated = $request->validate([
                    'title' => 'string',
                    'image' => 'mimes:jpeg,png',
                ]);

                $extension = $request->image->extension();
                $name_valide = str_replace(" ", "_", $validated['title']);
                $request->image->storeAs('/services', $name_valide.".".$extension, ['disk' => 'public']);
                $url = Storage::url($name_valide.".".$extension);
              
                
                $service->img_url = '/assets/img/services/'. $name_valide.".".$extension;


                $service->save();

                 $service_id = $service->id;

                
                foreach ($_POST['choix'] as $choix) {
                    
                    $FormService = new Form_Service();
                    $FormService->service_id = $service_id;
                    $FormService->formulaire_id = $choix;
                    $FormService->intitule = $_POST['label_'.$choix];
                    $FormService->default = $_POST['default_'.$choix];
                    $FormService->save();

                    
                    if(isset($_POST['type_serv_'.$choix]))
                    {
                        $listTS = explode(';',$_POST['type_serv_'.$choix]);
                        foreach ($listTS as  $tps) {
                            $tp = new TypeService();
                            $val = explode(':',$tps);
                            $tp->form_serv_id = $FormService->id;

                            $tp->libelle = $val[0];
                            $tp->prix_unitaire = $val[1];
                            $tp->save();
                        }
                        
                    }

                    
                    
                }
            }

        return redirect()->route('admin.services');
        }
    }



    function storeform(Request $request){
        $service = new Services();
        $service->title = $_POST['title'];
        $service->fee = $_POST['fee'];
        $service->status = 0;
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'title' => 'string',
                    'image' => 'mimes:jpeg,png',
                ]);
                //var_dump($validated);exit;

                $extension = $request->image->extension();
                $name_valide = str_replace(" ", "_", $validated['title']);
                $request->image->storeAs('/services', $name_valide.".".$extension, ['disk' => 'public']);
                $url = Storage::url($name_valide.".".$extension);
              
                
                $service->img_url = '/assets/img/services/'. $name_valide.".".$extension;

                $service->save();

                $service_id = $service->id;

                
                foreach ($_POST['choix'] as $choix) {
                    
                    $FormService = new Form_Service();
                    $FormService->service_id = $service_id;
                    $FormService->formulaire_id = $choix;
                    $FormService->intitule = $_POST['label_'.$choix];
                    $FormService->default = $_POST['default_'.$choix];
                    $FormService->save();

                    
                    if(isset($_POST['type_serv_'.$choix]))
                    {
                        $listTS = explode(';',$_POST['type_serv_'.$choix]);
                        foreach ($listTS as  $tps) {
                            $tp = new TypeService();
                            $val = explode(':',$tps);
                            $tp->form_serv_id = $FormService->id;
                            $tp->libelle = $val[0];
                            $tp->prix_unitaire = $val[1];
                            $tp->save();
                        }
                        
                    }

                    
                    
                }
                //return redirect()->route('admin.services');

                //return \Redirect::back();
            }
        }
        return redirect()->route('admin.services');
        //return \Redirect::back();
      
        
    }

    function publicites(){
        $publicites = Publicite::all();
        $form = $this->getFormPublicites();
        return view('admin.publicites',[ "publicites" => $publicites , 'form' => $form]);
    }

    function changestatus($id,$status){
        $service = Services::findOrFail($id);
        $service->status = $status;
        $service->save() ;

        return redirect()->route('admin.services');
    }

    function newpub(Request $request){
        $publicite = new Publicite();
        $publicite->title = $_POST['name'];
        $publicite->url = $_POST['url'];
        $publicite->description = $_POST['description'];
        $publicite->status = 1;
        $publicite->classes = "";
        $publicite->img_src = '';

        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'name' => 'string',
                    'image' => 'mimes:jpeg,png|dimensions:width=1200',
                ]);

                $extension = $request->image->extension();
                $name_valide = str_replace(" ", "_", $validated['name']);
                $une = $request->image->storeAs('/publicites', $name_valide.".".$extension, ['disk' => 'public']);
                
                $url = Storage::url($validated['name'].".".$extension);
              
                $publicite->img_src = '/assets/img/publicites/'. $name_valide .".".$extension;

              
            }
        }

        $publicite->save();
        return redirect()->route('admin.publicites');
    }

    function delpub($id){
        $publicite = Publicite::findOrFail($id)->delete();
        return redirect()->route('admin.publicites');
    }

    private  function getFormPublicites()
    {
        $opt = [
            'name' => "name",
            'label' => "Titre",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;
        $opt = [
            'name' => "image",
            'label' => "Image slider (Seul en dimension:1200x700)",
            'value' => "",
            'type' => "file"
        ];
        $options[]=$opt;

        $opt = [
            'name' => "url",
            'label' => "Lien de la publicité",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;
        $opt = [
            'name' => "description",
            'label' => "Resumé",
            'value' => "",
            'type' => "textarea"
        ];
        $options[]=$opt;

        $opt_btn = [
            'name' => "Enregistrer",
            'class' => "btn-primary",
            'type' => "submit"
        ];

        $options[]=$opt_btn;
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('admin.newpub'),
            ]);
        
        return $form;

    }

 }
