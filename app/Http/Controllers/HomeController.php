<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use App\Publicite;
use App\Orders;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;

use Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     private $formBuilder;
  
    public function __construct(FormBuilder $formBuilder)
    {
        $this->middleware('client');
     $this->formBuilder = $formBuilder;
    }
    function index(Request $request){
        if (Auth::check()) {
            if($request->session()->has('ToDoOrder'))
            {
                $orderid = $request->session()->get('ToDoOrder');
                $id = Auth::id();
                $order = Orders::find($orderid);
                $order->user_id = $id;
                $order->save();
                return redirect()->route('payement.pay');
            }
        }
        
        $services = Services::where('status', '=', 1)->get();

        $services_2_cols = array();

        $cpt = 0;
        $array_temp = array();
        foreach ($services as $service)
        {
            if($cpt<2){
                $array_temp[] = $service;
            }else{
                $services_2_cols[] = $array_temp;
                $array_temp = array();
                $array_temp[] = $service;
                $cpt = 0;
            }
            $cpt++;
        }
        if(!empty($array_temp)){
            $services_2_cols[] = $array_temp;
        }

       // $publicites = Publicite::all();
        $publicites = Publicite::where('status', '=', 1)->get();

        return view('home',[ "services_2_cols" => $services_2_cols, "publicites" => $publicites ]);
    }

    function orders(){
        $id = Auth::id();
        $orders = Orders::where('status', '=', 'Payé')
                        ->whereHas('user', function ($query) use ($id) {
                                $query->where('id', '=', $id)
                                      ->where(function($query) use ($id){
                                        $query->where('role', 'like', '%CLIENT%')
                                              ->orWhereNull('role');
                                    });
                            })
                        ->paginate(15);

        return view('orders',[ "orders" => $orders, 'status' => "Payé", "valsearch" => '' ]);
    }

    function conditionsGenerales(){
       
        return view('conditionsGenerales');
    }

     function mentionsLegales(){
       
        return view('mentionsLegales');
    }

    function faq(){
       
        return view('faq');
    }

    function send(Request $request){
         $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phonenumber' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

         $html = 'Nom : '.$_POST['subject'].'<br/>Numero Tel : '.$_POST['phonenumber'].'<br/>email : '.$_POST['email'] .'<br/>'.$_POST['message'];

         $data = [
            'subject' => $_POST['subject'],
            'email' => env('APP_ADM_MAIL',"florin.freelance@gmail.com"),
            'content' => $html
          ];
          if ($request->hasFile('file')) {

            $extension = $request->file->extension();
            $file = base64_encode(file_get_contents($request->file('file')->path()));
             $data = [
                'subject' => $_POST['subject'],
                'email' => env('APP_ADM_MAIL',"florin.freelance@gmail.com"),
                'content' => $html,
                'extension' => $extension,
                'file' => $file
              ];

           // print_r($file);exit;


            Mail::send('mail.contact-mail', $data, function($message) use ($data) {
                $message->to($data['email'])
                ->subject($data['subject'])
                ->attachData(base64_decode($data['file']),'piece_joint.'.$data['extension'])
                ->replyTo($_POST['email']);
              });
            //unlink($url);
          }else{
            Mail::send('mail.contact-mail', $data, function($message) use ($data) {
                $message->to($data['email'])
                ->subject($data['subject'])
                ->replyTo($_POST['email']);
              });
          }
  
          
          return redirect()->route('contact');


    }

    private function getFormContact()
    {
        $opt = [
            'name' => "name",
            'label' => "Nom Complet",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;
        $opt = [
            'name' => "email",
            'label' => "Email",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;

        $opt = [
            'name' => "phonenumber",
            'label' => "Numéro de Téléphone",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;

         $opt = [
            'name' => "subject",
            'label' => "Sujet",
            'value' => "",
            'type' => "text"
        ];
        $options[]=$opt;
        $opt = [
            'name' => "message",
            'label' => "Message",
            'value' => "",
            'type' => "textarea"
        ];
        $options[]=$opt;

        $opt = [
            'name' => "file",
            'label' => "Pièce jointe",
            'value' => "",
            'type' => "file"
        ];
        $options[]=$opt;

        $opt_btn = [
            'name' => "Suivant",
            'class' => "btn-primary",
            'type' => "submit"
        ];

        $options[]=$opt_btn;
        $form = $this->formBuilder->createByArray(
            $options
            ,[
                'method' => 'POST',
                'url' => route('send')
            ]);
        
        return $form;

    }

     function contact(){
       
        $form = $this->getFormContact();
       
       return view('contact', ['form'=>$form]);
    }


    function ordersby($ordre, $status, $input){
        $orders = Orders::where('status', '=', $status)->orderBy('created_at', $ordre)->paginate(15);
        if($input){
            $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($input){
                                $query->where('created_at', 'like', '%'.$input.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$input.'%')
                                      ->orWhere('montant', 'like', '%'.$input.'%')
                                      ->orWhere('frais', 'like', '%'.$input.'%')
                                      ->orWhere('montant_total', 'like', '%'.$input.'%')
                                      ->orWhere('quantity', 'like', '%'.$input.'%')
                                      ->orWhereHas('user', function ($query) use ($input) {
                                            $query->where('name', 'like', '%'.$input.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($input) {
                                            $query->where('title', 'like', '%'.$input.'%');
                                        });
                            })
                            ->orderBy('created_at', $ordre)
                            ->paginate(15);
        }
        return view('orders',[ "orders" => $orders, "status" => $status , "valsearch" => $input ]);
    }

    function ordersOperation($action, $value, $status){

        $valstatus = "Payé";
        $valsearch = "";
        $orders = Orders::where('status', '=', 'Payé')->paginate(15);
        $id = Auth::id();

        if($action == "status"){
            if($status != "tous"){
                $orders = Orders::where($action, '=', $status)
                            ->whereHas('user', function ($query) use ($id) {
                                $query->where('id', '=', $id)
                                      ->where(function($query) use ($id){
                                        $query->where('role', 'like', '%CLIENT%')
                                          ->orWhereNull('role');
                                    });
                            })
                            ->paginate(15);
              
                 if($value != "val"){
                    
                    $orders = Orders::where('status', '=', $status)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                             ->whereHas('user', function ($query) use ($id) {
                                $query->where('id', '=', $id)
                                      ->where(function($query) use ($id){
                                        $query->where('role', 'like', '%CLIENT%')
                                              ->orWhereNull('role');
                                });
                            })
                            ->paginate(15);
                        $valsearch = $value;
                    }
                
                $valstatus = $value;
            }else {
                $orders = Orders::whereHas('user', function ($query) use ($id) {
                                        $query->where('id', '=', $id)
                                             ->where(function($query) use ($id){
                                        $query->where('role', 'like', '%CLIENT%')
                                              ->orWhereNull('role');
                                        });
                                    })
                                    ->paginate(15);
                    if($value != "val"){
                        $orders = Orders::where(function($query) use ($value){
                                    $query->where('created_at', 'like', '%'.$value.'%')
                                          ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                          ->orWhere('montant', 'like', '%'.$value.'%')
                                          ->orWhere('frais', 'like', '%'.$value.'%')
                                          ->orWhere('montant_total', 'like', '%'.$value.'%')
                                          ->orWhere('quantity', 'like', '%'.$value.'%')
                                         ->orWhereHas('service', function ($query) use ($value) {
                                                $query->where('title', 'like', '%'.$value.'%');
                                            });
                                        })     
                                    ->whereHas('user', function ($query) use ($id) {
                                       $query->where('id', '=', $id)
                                            ->where(function($query) use ($id){
                                                 $query->where('role', 'like', '%CLIENT%')
                                                       ->orWhereNull('role');
                                            });
                                    })
                                    ->paginate(15);     
                        $valsearch = $value;
                    }
                }
                $valstatus = $status;
            }      
        if($action == "search"){
            $valstatus = $status;
            $valsearch = $value;
            $orders = array();

            $orders = Orders::where('status', '=', $valstatus)
                            ->where(function($query) use ($value){
                                $query->where('created_at', 'like', '%'.$value.'%')
                                      ->orWhere('dispatch_date', 'like', '%'.$value.'%')
                                      ->orWhere('montant', 'like', '%'.$value.'%')
                                      ->orWhere('frais', 'like', '%'.$value.'%')
                                      ->orWhere('montant_total', 'like', '%'.$value.'%')
                                      ->orWhere('quantity', 'like', '%'.$value.'%')
                                      ->orWhereHas('user', function ($query) use ($value) {
                                            $query->where('name', 'like', '%'.$value.'%');
                                        })
                                      ->orWhereHas('service', function ($query) use ($value) {
                                            $query->where('title', 'like', '%'.$value.'%');
                                        });
                            })
                            ->paginate(15);

        }


        return view('orders',[ "orders" => $orders, "status" => $valstatus, "valsearch" => $valsearch ]);
    }

    
 }
