<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_service';
    protected $primaryKey = 'id';
    public $incrementing = true;
}
