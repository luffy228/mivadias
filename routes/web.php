<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'HomeController@index')->name('home.index');
Route::get('/connection', function(){
    return view('auth.loginspecial');
})->name('loginspecial');


Route::get('formulaire/{id}', 'PaymentController@showFormulaire');
Route::get('choixAmount/{id}/{quantity}', 'PaymentController@choixAmount');
Route::post('formulaire/store/{id}', 'PaymentController@store')->name('payment.store');
Route::get('currency/{montant}', 'PaymentController@currency')->name('payment.currency');

Auth::routes(['verify' => true]);

Route::get('/conditionsGenerales', 'HomeController@conditionsGenerales')->name('conditionsGenerales');
Route::get('/mentionsLegales', 'HomeController@mentionsLegales')->name('mentionsLegales');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact/send', 'HomeController@send')->name('send');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/orders', 'HomeController@orders')->name('orders')->middleware('auth');
Route::get('/order/{id}', 'AdminController@orderclient')->name('order')->middleware('auth');
Route::get('/payement', 'PaymentController@MakePayementCinetPay')->name('payement.pay');
Route::get('/payement/cancel/{idorder}', 'PaymentController@CancelPay')->name('payement.cancel');
Route::post('/payement/callback/{idorder}', 'PaymentController@CallBackPayement')->name('payement.callback');

Route::get('/gerance', 'GeranceController@index')->name('gerance.index')->middleware('gerance');
Route::get('/gerance/orders', 'GeranceController@orders')->name('gerance.orders')->middleware('gerance');
Route::post('/gerance/validateorder/{id}', 'GeranceController@validateorder')->name('gerance.validateorder')->middleware('gerance');
Route::get('/gerance/order/{id}', 'GeranceController@order')->name('gerance.order')->middleware('gerance');

Route::get('/admin', 'AdminController@index')->name('admin.index')->middleware('admin');
Route::get('/admin/orders', 'AdminController@orders')->name('admin.orders')->middleware('admin');

Route::get('/admin/ordersby/{ordre}/{status}/{input}', 'AdminController@ordersby')->name('admin.ordersby')->middleware('admin');
Route::get('/admin/ordersOperation/{action}/{value}/{status}', 'AdminController@ordersOperation')->name('admin.ordersOperation')->middleware('admin');

Route::get('/gerance/ordersby/{ordre}/{status}/{input}', 'GeranceController@ordersby')->name('gerance.ordersby')->middleware('gerance');
Route::get('/gerance/ordersOperation/{action}/{value}/{status}', 'GeranceController@ordersOperation')->name('gerance.ordersOperation')->middleware('gerance');

Route::get('/ordersby/{ordre}/{status}/{input}', 'HomeController@ordersby')->name('home.ordersby')->middleware('auth');
Route::get('/ordersOperation/{action}/{value}/{status}', 'HomeController@ordersOperation')->name('home.ordersOperation')->middleware('auth');

Route::get('/admin/order/{id}', 'AdminController@order')->name('admin.order')->middleware('admin');

Route::get('/admin/geranceuser', 'AdminController@geranceuser')->name('admin.geranceuser')->middleware('admin');
Route::get('/admin/client', 'AdminController@client')->name('admin.client')->middleware('admin');
Route::post('/admin/newuser', 'AdminController@newuser')->name('admin.newuser')->middleware('admin');
Route::post('/admin/validateorder/{id}', 'AdminController@validateorder')->name('admin.validateorder')->middleware('admin');
Route::get('/admin/services', 'AdminController@services')->name('admin.services')->middleware('admin');
Route::get('/admin/service/{id}', 'AdminController@service')->name('admin.service')->middleware('admin');
Route::post('/admin/storeform', 'AdminController@storeform')->name('admin.storeform')->middleware('admin');
Route::post('/admin/modifservices/{id}', 'AdminController@modifservices')->name('admin.modifservices')->middleware('admin');


Route::get('/admin/publicites', 'AdminController@publicites')->name('admin.publicites')->middleware('admin');

Route::post('/admin/newpub', 'AdminController@newpub')->name('admin.newpub')->middleware('admin');
Route::post('/admin/delpub/{id}', 'AdminController@delpub')->name('admin.delpub')->middleware('admin');
Route::post('/admin/changestatus/{id}/{status}', 'AdminController@changestatus')->name('admin.changestatus')->middleware('admin');




Route::post('paiementstripe', 'StripePaymentController@stripe')->name("stripe.payement");
Route::post('payment', 'StripePaymentController@stripePost')->name('stripe.post');
